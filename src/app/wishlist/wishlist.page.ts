import { Component } from '@angular/core';
import { Events, ToastController, LoadingController, AlertController } from '@ionic/angular';
import { Router, ActivatedRoute } from "@angular/router";
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { ApiService } from '../service/api.service';
import { Location } from '@angular/common';

import { AppSettings } from './../urlset';
@Component({
  selector: 'app-wishlist',
  templateUrl: 'wishlist.page.html',
  styleUrls: ['wishlist.page.scss']
})
export class WishlistPage {  
    image_url:any=AppSettings.image_url;    
    upload_url:any=AppSettings.uploaded_image_url; 
    show_products_please_wait_message:boolean=false;
    wishlists:any=[];
    page:number=1;
    cartcount:number=0;
    hideInfiniteScroll:number=0;
    selected_all_products:string="false";
    delete_products_button:boolean=false;
    empty_data_message:boolean=false;
    
     constructor(
        private router: Router, 
        public events:Events, 
        private route: ActivatedRoute, 
        public alertController: AlertController,
        public toastController: ToastController, 
        public loadingController: LoadingController, 
        public http:Http, 
        public sanitizer: DomSanitizer,
        public api:ApiService,
        private _location: Location) {
    }
    
    getCartProductCounts(){
        let checkCart = JSON.parse(localStorage.getItem('cart'));
        if(checkCart && checkCart.products.length>=1){
            this.cartcount = checkCart.products.length;        
        }else{
            this.cartcount=0;
        }
    }
    
    //get all wishlists list of current uers's
    async get_wishlists(){   
        this.page=1;
        this.show_products_please_wait_message=true;            
        this.wishlists='';
        let loading = await this.loadingController.create({
        message: 'Please wait...',
        cssClass: 'custom-loading'
        });
        loading.present();
        let arrdata ={'page':this.page};
        this.api.postApi('get_wishlists',arrdata,true).then(res => {
            let response : any = res;
            loading.dismiss();
            this.show_products_please_wait_message=false;
            if (response.status == true) {    
                this.wishlists = response.wishlists;
            }
            if(this.wishlists.length<=0){
                this.empty_data_message=true;
            }else{
                this.empty_data_message=false;
            }
        });
    }
    
     //when scroll down then append next wishlist products
    async nextWishlistProducts(infiniteScroll){
        this.page++;          
        let arrdata ={'page':this.page};
        this.api.postApi('get_wishlists',arrdata,true).then(res => {
            let response : any = res;
            setTimeout(()=>{
                if (response.status == true) {    
                    this.wishlists = this.wishlists.concat(response.wishlists);
                    if(response.wishlists.length<=0){
                        this.hideInfiniteScroll=1;
                    }else{
                        this.hideInfiniteScroll=0;
                    }
                }  
                infiniteScroll.target.complete(); 
            },500);  
        });     
    } 
    
    //select products for delete 
    select_delete_product(type, selected_index){
        if(type=='all'){
            if(this.selected_all_products=='true'){
                this.selected_all_products='false';
                this.wishlists.forEach((product_details, index) => {    
                    product_details.delete_checked='false';
                });
                this.delete_products_button=false;       
            }else if(this.selected_all_products=='false'){
                this.selected_all_products='true';
                this.wishlists.forEach((product_details, index) => {    
                    product_details.delete_checked='true';
                }); 
            }
        }else{
            if (this.wishlists[selected_index].delete_checked=='false') {
                this.wishlists[selected_index].delete_checked='true';
            }else if (this.wishlists[selected_index].delete_checked=='true') {
                this.wishlists[selected_index].delete_checked='false';
                this.selected_all_products='false'; 
            }   
        }      
        
        this.selected_all_products='true';
        this.wishlists.forEach((product_details, index) => {
            if(product_details.delete_checked=='true'){
                this.delete_products_button=true;                      
            }
            if(product_details.delete_checked=='false'){
                this.selected_all_products='false';                 
            }
        });
    }

    //delete selected products from wishlist 
   async delete_selected_products(){
        if(this.delete_products_button){
          const alert = await this.alertController.create({
              header: 'Remove Product',
              message: 'Sure you want to remove selected products from wishlist?',
              buttons: [
                {
                  text: 'Cancel',
                  role: 'cancel',
                  cssClass: 'secondary',
                  handler: (blah) => {}
                }, {
                  text: 'Ok',
                  handler: () => {
                        let wishlist_ids=new Array();
                        for(let i = this.wishlists.length-1; i>=0; i--){
                            if(this.wishlists[i].delete_checked=='true'){                                    
                                wishlist_ids.push(this.wishlists[i].id);
                            }
                         }
                        if(wishlist_ids){
                            setTimeout(()=>{
                                this.remove_from_wishlist('multiple',wishlist_ids);
                            },200);
                        }
                    }
                }
              ]
            });
            await alert.present();
        }else{
            this.showMessage('failed', 'Please select atleast one product.');  
        }
    }
       
    //product remove from wishlist
    async remove_from_wishlist(remove_type, wishlist_id){  
        let loading = await this.loadingController.create({
        message: 'Please wait...',
        cssClass: 'custom-loading'
        });
        loading.present();

        let wishlist_ids=new Array();
        if(remove_type == 'single'){
            wishlist_ids.push(wishlist_id);
        }else{
            wishlist_ids=wishlist_id;
        }
        let data={'wishlist_ids': wishlist_ids};
        this.api.postApi('remove_from_wishlist',data,true).then(res => {
            let response : any = res;
            loading.dismiss();
            if (response.status == true) {      
                this.get_wishlists();   
                this.events.publish('updatehomePagedetails');
                this.showMessage('success', response.message);  
            }else{
                this.showMessage('failed', response.message);  
            }
        });        
    }  
    
    // show message into toast
    async showMessage(type, message) {
        if(type=='success'){
            let successtoast = await this.toastController.create({
                message: message,
                duration: 3000,
                cssClass:'success-toast'
            });
            successtoast.present();
        }else{
             let failedtoast = await this.toastController.create({
                message: message,
                duration: 3000,
                cssClass:'failed-toast'
            });
            failedtoast.present();
        }
    }
    
    //when this will load
    ionViewWillEnter(){       
        this.get_wishlists();
        this.getCartProductCounts();
    }
    // Back button
    setBackButtonAction(){
        this._location.back();
    }
}

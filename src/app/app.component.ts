import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, Events, LoadingController, ToastController, NavController, AlertController, IonRouterOutlet,ModalController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Router } from '@angular/router';
import { Push, PushObject, PushOptions } from '@ionic-native/push/ngx';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { Market } from '@ionic-native/market/ngx';
import { AppSettings } from './urlset';
import { Network } from '@ionic-native/network/ngx';
import { ApiService } from './service/api.service';
import * as $ from 'jquery'
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
    image_url:any=AppSettings.image_url;
    upload_url:any=AppSettings.uploaded_image_url;
    loggedInUser:boolean=false;
    googleAPIKey:any=AppSettings.googleAPIKey;
    appPages: Array<{title: string, url: string, icon: string, class:string,pagename:string}>;
    notification_details:any;
    userdetails:any={
      name:'',
      image:''
    }
    perchasedPlandpages:any;
    checking_permission:boolean=false;
    appStyle:boolean=false;
    @ViewChild(IonRouterOutlet) routerOutlet: IonRouterOutlet;
    backButtonSubscription;
    constructor(
      public api:ApiService,private modalCtrl: ModalController,private market: Market,private appVersion: AppVersion,private network:Network,private platform: Platform, private splashScreen: SplashScreen, private statusBar: StatusBar, public events:Events,
      public loadingController: LoadingController, public toastController: ToastController, public http:Http, public menuCtrl: MenuController,
      private router: Router, public nav: NavController, public alertController: AlertController, private push: Push,private androidPermissions:AndroidPermissions) {
      this.initializeApp();
      this.events.subscribe('checkLoggedInToken',(()=>{
          this.check_token_details();
      }));

      this.events.subscribe('internetConnection',(()=>{
        this.checkConnection();
      }));

      this.events.subscribe('appalycssonmainPage',(()=>{
        this.appalycssonmainPage();
      }));
      this.events.subscribe('getperchasedPlan',(()=>{
        this.getperchasedPlan();
      }));
      
    }

    ionViewDidEnter(){
      navigator['app'].clearHistory();    
    }
    appalycssonmainPage(){
      this.appStyle=false;
      if (this.router.url === '/home') {
        this.appStyle=true;
      }
    }
    initializeApp() {
      this.platform.ready().then(() => {
          this.splashScreen.hide();
          if(!this.checking_permission){
            this.androidPermissions.requestPermissions([
              this.androidPermissions.PERMISSION.CAMERA, 
              this.androidPermissions.PERMISSION.FOREGROUND_SERVICE, 
              this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, 
              this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE,
            ]);
          }  
          this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE).then(
            result => console.log('Has permission?', result.hasPermission),
            err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE)
          );
          
          this.checking_permission=true;  
           this.backButtonEvent();
          this.check_token_details();
          this.pushNotificationDetails();
          this.appPages = [
               {
                 title: 'Home',
                 url: '/home',
                 icon: 'home',
                 class:'',
                 pagename:''
               }
           ];
          this.loggedInUser=false;
         
          this.checkUpdateVersion();
      });
    }
    getAddressFromLatLng(latValue, lngValue){
      if(latValue && lngValue){
        let key = this.googleAPIKey;
        this.http.get('https://maps.googleapis.com/maps/api/geocode/json?latlng='+latValue+','+lngValue+'&key='+key).subscribe(data => {
            let response = JSON.parse(data['_body']);
            let address = response['results'][0]['formatted_address'];
            let location : any ={'lat':latValue, 'lng':lngValue, 'address':address};
            localStorage.setItem('currentLocation', JSON.stringify(location));
        }, error => {
            console.log(error);
        });
      }
   }
    //check version for update and show alert
    checkUpdateVersion(){
        /*var headers = new Headers();
        headers.append("Accept", 'application/json');
        headers.append('Content-Type', 'application/json');
        const requestOptions = new RequestOptions({ headers: headers });
        this.http.get(AppSettings.backend_url +'checkVersionUpdate', requestOptions).subscribe(data => {
          let response = JSON.parse(data['_body']);
          let playstoreVersion  =  response.version;
          this.appVersion.getVersionCode().then(value => {
            console.log("play store version",value);
            console.log("current version",playstoreVersion);
            if(playstoreVersion > value){
                this.updateVersion();
            }
          }).catch(err => {
              //alert(err);
          });
        }, error => {
            console.log(error);
        });*/
    }
    //show update version alert
    async updateVersion(){
        const alert = await this.alertController.create({
            header: 'New Version Available',
            message: 'There is newer version available for download! Please update the app by gotit.',
            buttons: [
              {
                text: 'Update',
                cssClass: 'btn-yes',
                handler: () => {
                   this.market.open('com.gotit.limra');
                }
              },
              {
                text: 'Cancel',
                role: 'cancel',
                cssClass: 'btn-no',
                handler: (blah) => {}
              }
            ]
        });
        await alert.present();
    }
    // watch network for a connection
    async checkConnection(){
      if (this.platform.is('cordova')) {
        if(this.network.type == 'none'){
          const alert = await this.alertController.create({
              header: 'Offline',
              message: 'Your network is unavailable. Check your data or wifi connection.',
              buttons: [
                {
                  text: 'Close',
                  role: 'cancel',
                  cssClass: 'btn-center',
                  handler: (blah) => {
                  }
                }
              ]
          });
          await alert.present();
          await this.loadingController.dismiss().then(() => console.log('dismissed'));
          return false;

        }
      }
    }
    getperchasedPlan(){
      let currentUser= JSON.parse(localStorage.getItem('currentUser'));
       if(currentUser!== 'undefined' && currentUser !== null){
        this.api.postApi('getperchasedPlan',{},true).then(res => {
          let response : any = res;
          console.log("response",res);
          if (response.status == true) {
            this.perchasedPlandpages=response.data;
          }
        });
      }
    }

   //do logout
    async logout(){
      let loading = await this.loadingController.create({
          message: 'Please wait...',
          cssClass: 'custom-loading'
      });
      loading.present();
      let currentUser= JSON.parse(localStorage.getItem('currentUser'));
      let data= {'token':currentUser.token};
      this.api.postApi('logout',data,true).then(res => {
        let response : any = res;
        loading.dismiss();
        if (response.status == true) {
            this.showMessage('success', response.message);
            localStorage.removeItem('currentUser');
            this.loggedInUser=false;
            this.check_token_details();
            this.router.navigateByUrl('/home');
        }else if (response.status == false) {
            this.showMessage('failed', response.message);
        }
      });
    }

    // show message into toast
    async showMessage(type, message) {
        if(type=='success'){
            let successtoast = await this.toastController.create({
                message: message,
                duration: 3000,
                cssClass:'success-toast'
            });
            successtoast.present();
        }else{
             let failedtoast = await this.toastController.create({
                message: message,
                duration: 3000,
                cssClass:'failed-toast'
            });
            failedtoast.present();
        }
    }

    //check token is available or not into databse
    check_token_details(){
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if(currentUser === 'undefined' || currentUser === null){
          this.appPages = [
            {
              title: 'Home',
              url: '/home',
              icon: 'home',
              class:'',
              pagename:''
            }
          ];

          this.router.navigateByUrl('/home');
        }else{
          let currentUser = JSON.parse(localStorage.getItem('currentUser'));
          let arrdata : any = {'token':currentUser.token};
          this.api.postApi('check_token_details',arrdata,true).then(res => {
            let response : any = res;
            if (response.status == true) {
              this.userdetails = response.user_details;
              this.loggedInUser=true;
              this.appPages = [
              {
                title: 'Home',
                url: '/home',
                icon: 'home',
                class:'',
                pagename:''
              },
              {
                title: 'Add Product',
                url: '/add-product',
                icon: 'add',
                class:'',
                pagename:''
              },
              {
                title: 'Manage Product',
                url: '/manage-products',
                icon: 'list',
                class:'',
                pagename:''
              },
              {
                title: 'Manage Category',
                url: '/category-list',
                icon: 'list',
                class:'',
                pagename:''
              }
              ];
              this.loggedInUser=true;
              this.updateDeviceID();
            }else{
              this.loggedInUser=false;
              localStorage.removeItem('currentUser');
              localStorage.clear();
              this.nav.navigateRoot('/home');
            }
          });
        }
    }
    //Update device id
    updateDeviceID(){
      let currentUser = JSON.parse(localStorage.getItem('currentUser'));
      let requestData = {'device_id':localStorage.getItem('deviceId'),'os':'android'};
      this.api.postApi('register_device_id',requestData,false).then(res => {
        console.log("register_device_id",localStorage.getItem('deviceId'))
        let response : any = res;
      });
    }
    //push notification function
    pushNotificationDetails(){
      this.push.createChannel({
        id: "Test Sound",
        description: "Custom Notification Sound",
        importance: 5,
        sound:'bell'
      }).then(() => console.log('Channel created')); 

      const options: PushOptions = {
        android: {
          senderID: '672427050176',
          sound:'bell',
          vibrate:false,
          forceShow:true
        },
        ios: {
           alert: 'true',
           badge: true,
           sound: 'false'
        },
        windows: {}
      }
      const pushObject: PushObject = this.push.init(options);
        pushObject.on('notification').subscribe((notification: any) => {
        console.log("notification data",notification);
          let additional_data = notification.additionalData;
          let notiID = additional_data.notId;
          let component = additional_data.component;
          let type_id = additional_data.type_id;
          let foreground = additional_data.foreground;
          if(!foreground){
            if(component=='productDetails'){
                this.router.navigateByUrl('/product-details/'+type_id);
            }else{
                this.router.navigateByUrl('/notifications');
            }
          }
      });

      pushObject.on('registration').subscribe((registration: any) =>{
          console.log("deviceId",registration.registrationId);
          localStorage.setItem('deviceId',registration.registrationId);
          
          let requestData = {'device_id':registration.registrationId,'os':'android'};
          this.api.postApi('register_device_id',requestData,false).then(res => {
            let response : any = res;
          });
      });

      pushObject.on('error').subscribe(error =>{ console.log('Error with Push plugin'+error); });
    }

    //GET NOTIFICATION DETAILS based on notification id
    get_notification_details(notification_id){
        let currentUser= JSON.parse(localStorage.getItem('currentUser'));
        var headers = new Headers();
        headers.append("Accept", 'application/json');
        headers.append('Content-Type', 'application/json' );
        headers.append('Authorization', 'Bearer ' + currentUser.token);
        const requestOptions = new RequestOptions({ headers: headers });

        let requestData = { 'notification_id': notification_id};

        this.http.post(AppSettings.backend_url +'get_notification_details', requestData, requestOptions).subscribe(data => {
            let response = JSON.parse(data['_body']);
            if (response.status == true) {
                this.notification_details= response.notification_details;
            }else{

            }
        }, error => {
            console.log(error);
        });
    }
    async openPage(icnt){
      let url=this.appPages[icnt].url;
      let currentUser= JSON.parse(localStorage.getItem('currentUser'));
      if(this.appPages[icnt].pagename!=''){
        let loading = await this.loadingController.create({
          message: 'Please wait...',
          cssClass: 'custom-loading'
        });
        loading.present();
        this.api.postApi('getperchasedPlan',{},true).then(res => {
          let response : any = res;
          loading.dismiss();
          console.log("response",res);
          if (response.status == true) {
            this.perchasedPlandpages=response.data;
            if(this.perchasedPlandpages.indexOf(this.appPages[icnt].pagename) !== -1){
               this.router.navigateByUrl(url);
            }else{
              this.showMessage('failed', "You need to upgrade plan");
              this.router.navigateByUrl('/subscription');
            }
          }
        });
      }else{
        this.router.navigateByUrl(url);
      }
      

    }
    //hardware back button
    async backButtonEvent(){
      this.backButtonSubscription = this.platform.backButton.subscribeWithPriority(1,() => {
        console.log("this is backbutton",this.router.url);
        if (this.router.url === '/home') {
          this.exitMainApp();
        }else if(this.routerOutlet && this.routerOutlet.canGoBack()) {
          this.routerOutlet.pop();
        }
      })
    }

     //exit from app
    async exitMainApp(){
        const alert = await this.alertController.create({
            header: 'Exit App',
            message: 'Do you want to exit the app?',
            buttons: [
              {
                text: 'Yes',
                cssClass: 'btn-yes',
                handler: () => {
                    navigator['app'].exitApp();
                }
              },
              {
                text: 'No',
                role: 'cancel',
                cssClass: 'btn-no',
                handler: (blah) => {}
              }
            ]
        });
        await alert.present();
    }

}

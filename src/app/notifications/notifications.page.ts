import { Component } from '@angular/core';
import { Events, ToastController, LoadingController, AlertController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Location } from '@angular/common';

import { AppSettings } from './../urlset';
@Component({
  selector: 'app-notifications',
  templateUrl: 'notifications.page.html',
  styleUrls: ['notifications.page.scss']
})
export class NotificationsPage {
    image_url:any=AppSettings.image_url;
    order_code:any=AppSettings.order_code;
    notifications:any=[];
    isshow : any = '0';
    page:number=1;
    showLoder : boolean = true;
    notification_details={
        id:'',
        title:'',
        description:''
    }
    constructor(private _location: Location, private router: Router, public http:Http, public events:Events, public toastController: ToastController, private route: ActivatedRoute,
    public loadingController: LoadingController, public alertController:AlertController) {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if(currentUser === 'undefined' || currentUser === null){
            this.router.navigateByUrl('/login');
        }else{
            this.events.publish('checkLoggedInToken');
            this.get_notifications();
            this.events.subscribe('updateNotificationsList',(()=>{
                this.get_notifications();
            }));
        }
    }

     //get all parcel orders of current user's
    async get_notifications(){
        let loading = await this.loadingController.create({
            message: 'Please wait...',
            cssClass: 'custom-loading'
        });
        loading.present();

        this.page=1;
        let currentUser= JSON.parse(localStorage.getItem('currentUser'));
        var headers = new Headers();
        headers.append("Accept", 'application/json');
        headers.append('Content-Type', 'application/json' );
        headers.append('Authorization', 'Bearer ' + currentUser.token);
        const requestOptions = new RequestOptions({ headers: headers });
        let data = {'page':this.page};
        this.events.publish('internetConnection');
        this.http.post(AppSettings.backend_url +'Gotitusermobile/get_notifications', data, requestOptions).subscribe(data => {
            let response = JSON.parse(data['_body']);
             loading.dismiss();
             this.isshow = 1;
            if (response.status == true) {
                this.notifications=response.notifications;
                let ids : string ="";
                for(let i in this.notifications){
                   //if(this.notifications[i].status=='unread')
                   {
                     ids =ids+","+this.notifications[i].id;
                   }
                }
               
                if(ids!=""){
                  this.update_notification_read_by(ids);
                }
            }
        }, error => {
            console.log(error);
            loading.dismiss();
        });
    }

    //when scroll down then append next notifications
    nextNotifications(infiniteScroll){
        setTimeout(() => {
            this.page++;
            let currentUser= JSON.parse(localStorage.getItem('currentUser'));
            var headers = new Headers();
            headers.append("Accept", 'application/json');
            headers.append('Content-Type', 'application/json' );
            headers.append('Authorization', 'Bearer ' + currentUser.token);
            const requestOptions = new RequestOptions({ headers: headers });
            let data = {'page':this.page};

            this.http.post(AppSettings.backend_url +'Gotitusermobile/get_notifications', data, requestOptions).subscribe(data => {
                let response = JSON.parse(data['_body']);
                if (response.status == true) {
                    this.notifications = this.notifications.concat(response.notifications);
                }
                infiniteScroll.target.complete();
            }, error => {
                console.log(error);
            });
        }, 500);
    }

    //GET NOTIFICATION DETAILS based on notification id
    openNotificationDetails(component, typeID, notification_id, status){
        if(component=='OrderDetailsPage'){
            this.router.navigateByUrl('/orderdetails/'+typeID);
        }
        if(status=='unread'){
            this.update_notification_read_by(notification_id);
        }
    }

    //GET NOTIFICATION DETAILS based on notification id
    update_notification_read_by(notification_id){
        let currentUser= JSON.parse(localStorage.getItem('currentUser'));
        var headers = new Headers();
        headers.append("Accept", 'application/json');
        headers.append('Content-Type', 'application/json' );
        headers.append('Authorization', 'Bearer ' + currentUser.token);
        const requestOptions = new RequestOptions({ headers: headers });

        let requestData = { 'notification_id': notification_id};
        this.events.publish('internetConnection');
        this.http.post(AppSettings.backend_url +'Gotitusermobile/update_notification_read_by', requestData, requestOptions).subscribe(data => {
            let response = JSON.parse(data['_body']);
            if (response.status == true) {
                this.notification_details= response.notification_details;
                //this.get_notifications();
                setTimeout(()=>{
                    this.events.publish('updateOrderTrackDetails');
                },500);
            }
        }, error => {
            console.log(error);
        });
    }
}

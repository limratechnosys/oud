import { ViewChild, Component, OnInit } from '@angular/core';
import { AppSettings } from './../urlset';
import { ApiService } from '../service/api.service';
import { LoadingController,Events,ToastController, IonSlides} from '@ionic/angular';
import { Router, ActivatedRoute } from "@angular/router";
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { Location } from '@angular/common';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.page.html',
  styleUrls: ['./product-details.page.scss'],
})
export class ProductDetailsPage{
  @ViewChild('mySlider')  slides: IonSlides
  @ViewChild('relatedslider')  relatedslider: IonSlides
	image_url:any=AppSettings.image_url;
	upload_url:any=AppSettings.uploaded_image_url;
  type:any='';
  	productid:any;
  	public product_details={
        id:'',
        categoryname:'',
        model_no:'',
        image:'',
        name:'',
        review:'',
        discountprice:0,
        discount:'',
        price:'',
        description:'',
        feature_detail:'',
        details:'',
        total_price:0,
        quantity:0,
        stock:0,
        stockname:'',
        variant:[],
        variant_range:'',
        selected_variant:0,
        images:[],
        reviews:[],
        tax_type:'',
        wishlist:0,
        brandname:'',        
        show_net_weight:0,
        product_by:'',
        profile_image:'',
        login_user_id:'',
        user_id:'',
        prev:0,
        next:0,
        category:'',
        subcategory:'',
        dynamic_fields:[],
        inner_qty:'',
        outer_qty:''
    }
    sliderConfig = {
    	slidesPerView: 1,
  	};
    multiplesliderConfig = {
      slidesPerView: 3,
      spaceBetween: 12,
      centeredSlides: false,
      autoplay:4
    };
    productsliderConfig = {
      slidesPerView: 2.8,
      spaceBetween: 12,
      centeredSlides: false,
      autoplay:4
    };
    relatedproducts:any=[];
    Is_stock:boolean=true;
    review_products:any=[];
    currentQty:any='';
  constructor(
  	public api:ApiService,
  	public loadingController: LoadingController,
  	private route: ActivatedRoute,
  	public sanitizer: DomSanitizer,
  	private socialSharing: SocialSharing,
  	private router: Router,
  	public events:Events,
  	public toastController: ToastController,
    private _location: Location,
    private photoViewer: PhotoViewer) { 
  	this.route.params.subscribe( params => {
        this.productid = params.productId;
        this.type = params.type;
    });
  	
    let cart = JSON.parse(localStorage.getItem('cart'));
    if (cart!== null) {
      this.review_products=cart;
      let index = this.review_products.findIndex((item) => item.id === this.productid);
      if(index>=0){
        this.currentQty=this.review_products[index].qty;
      }
    }
  }
  updateQty(events){
    var id=this.productid;
    if(events>0){
      let index = this.review_products.findIndex((item) => item.id === id);
      if(index>=0){
        this.review_products[index]={
          id:id,
          name:this.product_details.name,
          model_no:this.product_details.model_no,
          qty:events,
          image:this.upload_url+this.product_details.images[0].images
        };
      }else{
        this.review_products.push({
          id:id,
          name:this.product_details.name,
          model_no:this.product_details.model_no,
          qty:events,
          image:this.upload_url+this.product_details.images[0].images
        });
      }
      this.currentQty=events;
    }else{
      let index = this.review_products.findIndex((item) => item.id === id);
      if(index>=0){
        this.review_products.splice(index, 1);
      }
      this.currentQty=events;
    }

    var cart = JSON.stringify(this.review_products);
    localStorage.setItem('cart', cart);

    setTimeout(()=>{
      this.events.publish('updateCartCount');
    },500);
  }
  
  slidePrev(type){
    if(type=='related'){
      this.relatedslider.slidePrev();
    }else{
      this.slides.slidePrev();
    }
  }
  slideNext(type){
    if(type=='related'){
      this.relatedslider.slideNext();
    }else{
      this.slides.slideNext();
    }

    
  }

  prevLoad(id){
    this.productid=id;
    this.getproductDetails();
  }
  nextLoad(id){
    this.productid=id;
    this.getproductDetails();
  }
  
  //Get related product details
  async getrelatedproducts(){
    let loading = await this.loadingController.create({
      message: 'Please wait...',
      cssClass: 'custom-loading'
    });
    loading.present();
    this.api.postApi('get_related_products',{product_id:this.productid},true).then(res => {
        let response : any = res;
        loading.dismiss();
        if (response.status == true) {
          this.relatedproducts=response.related_products;
        }
    }); 
  }

  changeImage(image){
    this.product_details.image=image;
  }
	//Get product details
	async getproductDetails(){
		let loading = await this.loadingController.create({
	    message: 'Please wait...',
	    cssClass: 'custom-loading'
		});
		loading.present();
		this.api.postApi('get_product_details',{id:this.productid,type:this.type},true).then(res => {
		    let response : any = res;
		    loading.dismiss();
		    if (response.status == true) {
		    	this.product_details=response.product_details;
          this.product_details.image=this.product_details.variant[0].image;
          if(this.product_details.stock==0){
            this.Is_stock=false;
          }
		    }
		}); 
	}

  	//when this will load
    ionViewWillEnter(){
        setTimeout(()=>{
            this.getproductDetails();
            this.getrelatedproducts();
        },300);
    }
     //share product details
    share_product_details(){
        let file = this.upload_url+this.product_details.variant[0].image;
        let productname = "Check this out "+this.product_details.name;
        let url = this.upload_url+"ProductDetails/" + this.product_details.id;
        this.socialSharing.share(productname, null, file, url);
    }

    //add into wishlist
    async add_to_wishlist(product_id){
        let loading = await this.loadingController.create({
            message: 'Please wait...',
            cssClass: 'custom-loading'
        });
        loading.present();
        let data={ 'id': product_id};
        this.api.postApi('add_to_wishlist',data,true).then(res => {
		    let response : any = res;
		    loading.dismiss();
		    if (response.status == true) {
            this.events.publish('updatehomePagedetails');
            this.showMessage('success', response.message);
            this.product_details.wishlist=1;
        }else{
            this.showMessage('failed', response.message);
        }
		});
    }
    // show message into toast
    async showMessage(type, message) {
        if(type=='success'){
            let successtoast = await this.toastController.create({
                message: message,
                duration: 3000,
                cssClass:'success-toast'
            });
            successtoast.present();
        }else{
             let failedtoast = await this.toastController.create({
                message: message,
                duration: 3000,
                cssClass:'failed-toast'
            });
            failedtoast.present();
        }
    }
    // Back button
    setBackButtonAction(){
      this._location.back();
    }
  // view image
   viewImage(image){
      let imagePath = this.upload_url+image;
      this.photoViewer.show(imagePath);
   }
}

import { Component, OnInit } from '@angular/core';
import { Events, ToastController, ActionSheetController, LoadingController, AlertController,ModalController } from '@ionic/angular';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { AppSettings } from './../urlset';
import { ApiService } from '../service/api.service';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { DomSanitizer } from '@angular/platform-browser';
import { Base64 } from '@ionic-native/base64/ngx';
import { Http, Response, RequestOptions, Headers} from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { AddcategoryPage } from '../addcategory/addcategory.page';
@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.page.html',
  styleUrls: ['./add-product.page.scss'],
})
export class AddProductPage implements OnInit {
    product_id:any;
	image_url:any=AppSettings.image_url;
    upload_url:any=AppSettings.uploaded_image_url;
	collectForm: FormGroup;
    arrcategory:any=[];
    showError:boolean=false;
    arrcollect={
        product_id:0,
        name:'',
        model_no:'',
        in_stock:false,
        category_id:0,
        is_live:false,
        photos:'',
        description:'',
        inner_qty:'',
        outer_qty:'',
        stock:'',
        make_live:''
    }
    arrPhotos:any=[{
            path :'assets/icon/upload.png',
        },
        {
            path :'assets/icon/upload.png',
        }
    ];
    arrPhotosserver:any=[{
            image :'',
        },
        {
            image :'',
        }
    ];
	constructor(private camera: Camera,
		public actionSheetController: ActionSheetController,
		public loadingController: LoadingController,
		private sanitizer: DomSanitizer,
		private base64: Base64,
		public toastController: ToastController,
		public formBuilder: FormBuilder,
		public events:Events,
		public api:ApiService,
		private router: Router,
        private _location: Location,
        public alertController: AlertController,
        private route: ActivatedRoute,
        public modalCtrl: ModalController) { 
        this.route.params.subscribe( params => {
            this.product_id = params.id;    
            
        }); 
        this.collectForm = formBuilder.group({
            'name':['',Validators.compose([Validators.required])],
            'model_no':['',Validators.compose([Validators.required])],
            'category_id':['',Validators.compose([Validators.required])],
            'description':['',Validators.compose([Validators.required])],
            'inner_qty':['',Validators.compose([Validators.required])],
            'outer_qty':['',Validators.compose([Validators.required])],
            'in_stock':[''],
            'is_live':['']
        });
        this.getCategory();

        setTimeout(()=>{
            if(this.product_id>0){
                this.getproductDetails();
            }
        },700);
	}

    //Get product details
    async getproductDetails(){
        let loading = await this.loadingController.create({
        message: 'Please wait...',
        cssClass: 'custom-loading'
        });
        loading.present();
        this.api.postApi('get_product_details',{id:this.product_id},true).then(res => {
            let response : any = res;
            loading.dismiss();
            console.log("response",response)
            if (response.status == true) {
                this.arrcollect=response.product_details;
                this.arrPhotos =response.product_details.variant; 
            }
        }); 
    }

    async addProduct(){
        if((this.arrPhotosserver[0].image=="") && this.product_id==''){
            this.showMessage('error', "Please upload at least one image");
            return false;
        }

        let loading = await this.loadingController.create({
            message: 'Please wait...',
            cssClass: 'custom-loading'
        });
        loading.present();
        this.arrcollect.photos=JSON.stringify(this.arrPhotosserver);
        this.arrcollect.product_id = this.product_id;
        this.arrcollect.stock = (this.arrcollect.in_stock)==true?'1':'0';
        this.arrcollect.make_live = (this.arrcollect.is_live)==true?'1':'0';
        this.api.postApi('seveproductDetails',this.arrcollect,true).then(res => {
            let response : any = res;
            console.log("response",response);
            loading.dismiss();
            if (response.status == true) {
                if(this.product_id>0){
                    this.router.navigateByUrl('/manage-products');
                }else{
                    this.router.navigateByUrl('/home');
                }
                this.showMessage('success',response.message);
            }else{
                this.showMessage('error',response.message);
            }
        });     
    }
    //Get category
    async getCategory(){
        let loading = await this.loadingController.create({
            message: 'Please wait...',
            cssClass: 'custom-loading'
        });
        loading.present();
        this.api.postApi('get_categories',{},true).then(res => {
            let response : any = res;
            loading.dismiss();
            if (response.status == true) {
                this.arrcategory = response.categories;
            }
        });     
    }

    async openMyModal() {
        const myModal = await this.modalCtrl.create({
          component: AddcategoryPage,
          cssClass: 'my-custom-modal-css'
        });
        myModal.onDidDismiss().then((data) => {
            if(data.data.dismissed){
                this.getCategory();
            }
        });
        return await myModal.present();
    }
  
    addMorephoto(){
        this.arrPhotos.push({
            path :'assets/icon/upload.png',
        });
        this.arrPhotosserver.push({
            image :'',
        });
    }
    removePhoto(i){
        this.arrPhotos.splice(i, 1);
        this.arrPhotosserver.splice(i, 1);
    }
	//choose option for upload image
    async uploadImage(icnt) {
        const actionSheet = await this.actionSheetController.create({
          header: 'Picture',
          buttons: [{
            text: 'Camera',
            icon: 'camera',
            handler: () => {
              this.selectImage(this.camera.PictureSourceType.CAMERA,icnt);
            }
            },
            {
                text: 'Gallery',
                icon: 'albums',
                handler: () => {
                    this.selectImage(this.camera.PictureSourceType.PHOTOLIBRARY,icnt);
                }
              },
            {
            text: 'Cancel',
            icon: 'close',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          }]
        });
        await actionSheet.present();
    }

    //select image from mobile
     selectImage(type,icnt){
        const options: CameraOptions = {
            quality: 100,
            targetWidth: 500,
      		targetHeight: 500,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            sourceType:type,
            allowEdit: true,
            correctOrientation : true
            //saveToPhotoAlbum: true
        }
		this.camera.getPicture(options).then((imageData) => {
            let base64Image = 'data:image/jpeg;base64,' + imageData;
            this.arrPhotos[icnt].path=this.sanitizer.bypassSecurityTrustUrl(base64Image);
            this.arrPhotosserver[icnt].image=imageData;
            this.showMessage('success', "Photo uploaded successfully!");
            
		}, (err) => {
		    //this.showMessage('failed', err);
		});
    }

    // show message into toast
    async showMessage(type, message) {
        if(type=='success'){
            let successtoast = await this.toastController.create({
                message: message,
                duration: 3000,
                cssClass:'success-toast'
            });
            successtoast.present();
        }else{
             let failedtoast = await this.toastController.create({
                message: message,
                duration: 3000,
                cssClass:'failed-toast'
            });
            failedtoast.present();
        }
    }
    
	ngOnInit() {
	}
    // Back button
    setBackButtonAction(){
      this._location.back();
    }
}

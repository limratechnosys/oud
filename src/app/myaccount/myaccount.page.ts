import { Component } from '@angular/core';

import { AppSettings } from './../urlset';

@Component({
  selector: 'app-myaccount',
  templateUrl: 'myaccount.page.html',
  styleUrls: ['myaccount.page.scss'],
})
export class MyAccountPage {
    image_url:any=AppSettings.image_url;   
    
    constructor() { }
}

import { Component, ViewChild } from '@angular/core';
import { Events,LoadingController,ToastController,AlertController} from '@ionic/angular';
import { AppSettings } from './../urlset';
import { ApiService } from '../service/api.service';
import { Location } from '@angular/common';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';

/*import { PostPopover } from './post-popover';
import { Messages } from '../messages/messages';
*/
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  image_url:any=AppSettings.image_url;
  upload_url:any=AppSettings.uploaded_image_url;
  loginUserid:number=0;
  banner_details={
    top_banner:'',
    middle_banner:''
  }
  arrdetails={
    categories:[],
    products:[]
  };
  slideOptsOne = {
    initialSlide: 0,
    slidesPerView: 1,
    autoplay:true
  };
  slideOptsTwo = {
    initialSlide: 0,
    slidesPerView: 1,
    autoplay:true,
    speed: 1000
  };
  sliderConfig = {
    slidesPerView: 4.5,
    spaceBetween: 2,
    centeredSlides: false
  };
  hideInfiniteScroll:number=0;
  notificationCnt:number=0;
  page:number=1;   
  moreRecords:any=[];
  type:any="";
  showLoader:boolean=true;
  searchdata:any="";
  review_products:any=[];
  constructor(
    public api:ApiService,    
    public loadingController: LoadingController,
    public events:Events,
    public toastController: ToastController,
    private _location: Location,
    private socialSharing: SocialSharing,
    public sanitizer: DomSanitizer,
    private route: ActivatedRoute,
    private router: Router,
    public alertController: AlertController) {
    this.route.params.subscribe( params => {
        this.type = params.type;  
    });

    this.updateCart();
    this.events.subscribe('updateCartCount',(()=>{
      this.updateCart();
    }));

    this.gethomepageDetails();
    this.events.subscribe('updatenotificationCount',(()=>{
      this.gethomepageDetails();
    }));

  }
  gethomepageDetails(){
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    let token=false;
    console.log("currentUser",currentUser);
    if(currentUser !== 'undefined' && currentUser !== null){
      token=true;
    }
    //alert(token)
    this.api.postApi('gethomepageDetails',{token:token},true).then(res => {
      let response : any = res;
      if(response.status){
        this.notificationCnt=response.data.notificationCnt;
        console.log("response",response);
      }
    });
  }

  searchProducts(){
    this.showLoader=false;
    this.getproducts();
  }

  share(item){
    let file = this.upload_url+item.images[0].images;
    let url = "https://play.google.com/store/apps/details?id=com.oud.limra";
    let productname = "Check this out "+item.name;
    this.socialSharing.share(productname, null, file, url);
  }
  //update cart details on local storage
  updateCart() {
    let cart = JSON.parse(localStorage.getItem('cart'));
    if (cart!== null) {
      this.review_products=cart;
    }else{
      this.review_products=[];
    }
    
    for(let cartProduct of this.review_products) {
      let index = this.arrdetails.products.findIndex((item) => item.id === cartProduct.id);
      if(index>=0){
        this.arrdetails.products[index].qty=cartProduct.qty;
      }
    }
  };

  updateQty(events,details){
    let id=details.id;
    if(events>0){
      let index = this.review_products.findIndex((item) => item.id === id);
      if(index>=0){
        this.review_products[index]={
          id:id,
          name:details.name,
          model_no:details.model_no,
          qty:events,
          image:this.upload_url+details.images[0].images
        };
      }else{
        this.review_products.push({
          id:id,
          name:details.name,
          model_no:details.model_no,
          qty:events,
          image:this.upload_url+details.images[0].images
        });
      }
    }else{
      let index = this.review_products.findIndex((item) => item.id === id);
      if(index>=0){
        this.review_products.splice(index, 1);
      }
    }
    var cart = JSON.stringify(this.review_products);
    localStorage.setItem('cart', cart);
    setTimeout(()=>{
      this.events.publish('updateCartCount');
    },500);

  }
  async getproducts(){
    this.page=1;
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    let token=false;
    if(currentUser !== 'undefined' && currentUser !== null){
      token=true;
    }
    let loading;
    if(this.showLoader){
      loading = await this.loadingController.create({
        message: 'Please wait...',
        cssClass: 'custom-loading'
      });
      loading.present();
    }
    let data ={'page':this.page,'searchdata':this.searchdata};
    this.api.postApi('get_products',data,true).then(res => {
        console.log("datadddddddddd",res);
        let response : any = res;
        if(this.showLoader){
          loading.dismiss();
        }
        console.log("response",response)
        if (response.status == true) {
          this.arrdetails.categories=response.categories;
          this.arrdetails.products=response.products;
          this.banner_details =response.banner_details;
          this.updateCart();
          //this.loginUserid=response.uid;

        }
    }); 
  }
  searchreacord(data){
    this.showLoader=false;
    this.searchdata=data;
    this.getproducts();
  }


  /* //when scroll down then append next banner products
   nexttimeList(infiniteScroll){
        this.page++;               
        let data ={'page':this.page,'searchdata':this.searchdata};
        this.api.postApi('getproducts',data,true).then(res => {
        let response : any = res;
        console.log("response nexttimeList",response)
        if (response.status == true) {
          this.moreRecords=response.timelist;
                this.arrdetails.timelist = this.arrdetails.timelist.concat(response.timelist);
                if(response.timelist.length<=0){
                    this.hideInfiniteScroll=1;
                }else{
                    this.hideInfiniteScroll=0;
                }
        }
        infiniteScroll.target.complete(); 
    }); 
    }


   //share product details
    share_product_details(icnt){
        let file = this.upload_url+this.arrdetails.timelist[icnt].image;
        let productname = "Check this out "+this.arrdetails.timelist[icnt].name;
        let url = this.upload_url+"ProductDetails/" + this.arrdetails.timelist[icnt].id;
        this.socialSharing.share(productname, null, file, url);
    }*/

  
  // show message into toast
    async showMessage(type, message) {
        if(type=='success'){
            let successtoast = await this.toastController.create({
                message: message,
                duration: 3000,
                cssClass:'success-toast'
            });
            successtoast.present();
        }else{
             let failedtoast = await this.toastController.create({
                message: message,
                duration: 3000,
                cssClass:'failed-toast'
            });
            failedtoast.present();
        }
    }
    
  
  // Back button
    setBackButtonAction(){
      this._location.back();
    }
    ionViewWillEnter(){
      this.getproducts();
    }
    
    //add-product/{{item.id}}/copy

}
















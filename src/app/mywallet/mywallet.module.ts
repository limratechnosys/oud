import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';

import { MyWalletPage } from './mywallet.page';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    HttpModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: MyWalletPage
      }
    ])
  ],
  declarations: [MyWalletPage]
})
export class MyWalletPageModule {}

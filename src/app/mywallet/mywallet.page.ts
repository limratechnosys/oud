import { Component } from '@angular/core';
import { Events, LoadingController, ToastController, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

import { AppSettings } from './../urlset';

@Component({
  selector: 'app-mywallet',
  templateUrl: 'mywallet.page.html',
  styleUrls: ['mywallet.page.scss']
})

export class MyWalletPage{
    wallet_transactions:any=[];
    page:number=1;
    wallet_amount:any;
    wallet_recharge_amount:any;
    order_code:any=AppSettings.order_code;
    clearObservable:any;
    browser:any;

    constructor(private router: Router, public http:Http, public events:Events, public loadingController: LoadingController,
        public toastController: ToastController, public alertController: AlertController, private iab: InAppBrowser) {
        this.events.publish('checkLoggedInToken');
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if(currentUser === 'undefined' || currentUser === null){
            this.router.navigateByUrl('/login');
        }else{
            this.get_mywallet();
        }
    }
   //get all my wallet details of current user's
    async get_mywallet(){
        this.events.publish('internetConnection');
        let loading = await this.loadingController.create({
            message: 'Please wait...',
            cssClass: 'custom-loading'
        });
        loading.present();

        this.page=1;
        let currentUser= JSON.parse(localStorage.getItem('currentUser'));
        var headers = new Headers();
        headers.append("Accept", 'application/json');
        headers.append('Content-Type', 'application/json' );
        headers.append('Authorization', 'Bearer ' + currentUser.token);
        const requestOptions = new RequestOptions({ headers: headers });
        let data = {'page':this.page};
        this.http.post(AppSettings.backend_url +'Gotitusermobile/get_mywallet', data, requestOptions).subscribe(data => {
            let response = JSON.parse(data['_body']);
            console.log("response",response);
            loading.dismiss();
            if (response.status == true) {
                this.wallet_transactions=response.wallet_transactions;
                this.wallet_amount=response.wallet_amount;
            }
        }, error => {
            console.log(error);
        });
    }

    //when scroll down then append next wallet transaction
    nextWalleTranscation(infiniteScroll){
        setTimeout(() => {
            this.page++;
            let currentUser= JSON.parse(localStorage.getItem('currentUser'));
            var headers = new Headers();
            headers.append("Accept", 'application/json');
            headers.append('Content-Type', 'application/json' );
            headers.append('Authorization', 'Bearer ' + currentUser.token);
            const requestOptions = new RequestOptions({ headers: headers });
            let data = {'page':this.page};

            this.http.post(AppSettings.backend_url +'Commonmobile/get_mywallet', data, requestOptions).subscribe(data => {
                let response = JSON.parse(data['_body']);
                if (response.status == true) {
                    this.wallet_transactions = this.wallet_transactions.concat(response.wallet_transactions);
                }
                infiniteScroll.target.complete();
            }, error => {
                console.log(error);
            });
        }, 500);
    }

    //recharge wallet
    async recharge_wallet(){
        if(this.wallet_recharge_amount){
            let loading = await this.loadingController.create({
                message: 'Please wait...',
                cssClass: 'custom-loading'
            });
            loading.present();

            let currentUser= JSON.parse(localStorage.getItem('currentUser'));
            var headers = new Headers();
            headers.append("Accept", 'application/json');
            headers.append('Content-Type', 'application/json' );
            headers.append('Authorization', 'Bearer ' + currentUser.token);
            const requestOptions = new RequestOptions({ headers: headers });
            let request_data = {'wallet_recharge_amount':this.wallet_recharge_amount};
            this.events.publish('internetConnection');
            this.http.post(AppSettings.backend_url +'Gotitusermobile/recharge_wallet', request_data, requestOptions).subscribe(data => {
              loading.dismiss();
              let response = JSON.parse(data['_body']);
              if (response.status == true) {
                  this.browser = this.iab.create(response.url,'_blank','location=yes, hardwareback=no');
                  this.clearObservable = setInterval(()=> {
                      this.check_wallet_recharge(response.payment_id);
                  },3000);
              }else{
                  this.showMessage('failed', response.message);
              }
            }, error => {
                loading.dismiss();
                console.log(error);
            });
        }else{
            this.showMessage('failed','Please enter wallet recharge amount.');
        }
    }


     //check order payment response
    check_wallet_recharge(payment_id){
        let currentUser= JSON.parse(localStorage.getItem('currentUser'));
        var headers = new Headers();
        headers.append("Accept", 'application/json');
        headers.append('Content-Type', 'application/json' );
        headers.append('Authorization', 'Bearer ' + currentUser.token);
        const requestOptions = new RequestOptions({ headers: headers });
        let request_data ={'payment_id':payment_id};

        this.http.post(AppSettings.backend_url +'Gotitusermobile/check_wallet_recharge', request_data, requestOptions).subscribe(data => {
            let response = JSON.parse(data['_body']);
            if (response.status == true) {
                //clear observation and close payment page
                this.wallet_recharge_amount='';
                clearInterval(this.clearObservable);
                this.browser.close();

                this.get_mywallet();
                this.showMessage('success', "Wallet Recharged Successfully!");

            }else if (response.status == false) {
                //clear observation and close payment page
                clearInterval(this.clearObservable);
                this.browser.close();

                this.get_mywallet();
                this.showMessage('failed', "Wallet Recharge Failed.");
            }
        }, error => {
            console.log(error);
        });
    }

      // show message into toast
    async showMessage(type, message) {
        if(type=='success'){
            let successtoast = await this.toastController.create({
                message: message,
                duration: 3000,
                cssClass:'success-toast'
            });
            successtoast.present();
        }else{
             let failedtoast = await this.toastController.create({
                message: message,
                duration: 3000,
                cssClass:'failed-toast'
            });
            failedtoast.present();
        }
    }
}

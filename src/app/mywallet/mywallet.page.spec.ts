import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, async } from '@angular/core/testing';

import { MyWalletPage } from './mywallet.page';

describe('MyWalletPage', () => {
  let component: MyWalletPage;
  let fixture: ComponentFixture<MyWalletPage>;
  let MyWalletPage: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyWalletPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(async () => {
    fixture = await TestBed.createComponent(MyWalletPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a mywallet of 10 elements', () => {
    MyWalletPage = fixture.nativeElement;
    const items = MyWalletPage.querySelectorAll('ion-item');
    expect(items.length).toEqual(10);
  });

});

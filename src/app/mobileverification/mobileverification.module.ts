import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { MobileVerificationPage } from './mobileverification.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    FormsModule,
    HttpModule,
    RouterModule.forChild([
      {
        path: '',
        component: MobileVerificationPage
      }
    ])
  ],
  declarations: [MobileVerificationPage]
})
export class MobileVerificationPageModule {}
import { Component } from '@angular/core';
import { Events, LoadingController, ToastController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Location } from '@angular/common';
import { ApiService } from '../service/api.service';

import { AppSettings } from './../urlset';

@Component({
  selector: 'app-mobileverification',
  templateUrl: 'mobileverification.page.html',
  styleUrls: ['mobileverification.page.scss'],
})

export class MobileVerificationPage {
    image_url:any=AppSettings.image_url;
    mobileverificationForm: FormGroup;
    mobileverification={
        id:'',
        otp:''
    }

    constructor(public api:ApiService,private router: Router, public http:Http, formBuilder: FormBuilder, public events:Events, public loadingController: LoadingController,
    public toastController: ToastController, private route: ActivatedRoute, private _location: Location) {
        this.route.params.subscribe( params => {
            this.mobileverification.id=params.userID;
        });
        this.mobileverificationForm = formBuilder.group({
            'otp':['',Validators.compose([Validators.required, Validators.minLength(4)])],
        });
    }
    //resend OTP for mobile verification
    async resend_otp(){
        this.events.publish('internetConnection');
        let loading = await this.loadingController.create({
            message: 'Please wait...',
            cssClass: 'custom-loading'
        });
        loading.present();
        let data={ 'id' : this.mobileverification.id };
        this.api.postApi('resendMobileVerificationOTP',data).then(res => {
            let response : any = res;
            loading.dismiss();
            if (response.status == true) {
                this.showMessage('success', response.message);
            }else{
                this.showMessage('failed', response.message);
            }
        });
    }

     //verification of mobile number
    async verify_mobile(){
      this.events.publish('internetConnection');

        let loading = await this.loadingController.create({
            message: 'Please wait...',
            cssClass: 'custom-loading'
        });
        loading.present();
        this.api.postApi('verify_user',this.mobileverification).then(res => {
            let response : any = res;
            loading.dismiss();
            if (response.status == true) {
                console.log("response",response);
                localStorage.setItem('currentUser', JSON.stringify({ token: response.token }));
                this.showMessage('success', response.message);
                //this._location.back();
                setTimeout(()=>{
                    this.events.publish('checkLoggedInToken');
                    this.router.navigateByUrl('/home');
                },300);
            }else{
                this.showMessage('failed', response.message);
            }
        });
    }

     // show message into toast
    async showMessage(type, message) {
        if(type=='success'){
            let successtoast = await this.toastController.create({
                message: message,
                duration: 3000,
                cssClass:'success-toast'
            });
            successtoast.present();
        }else{
             let failedtoast = await this.toastController.create({
                message: message,
                duration: 3000,
                cssClass:'failed-toast'
            });
            failedtoast.present();
        }
    }

    //close current Page
    closeCurrentPage(){
        this._location.back();
    }

}
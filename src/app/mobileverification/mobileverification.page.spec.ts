import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MobileverificationPage } from './mobileverification.page';

describe('MobileverificationPage', () => {
  let component: MobileverificationPage;
  let fixture: ComponentFixture<MobileverificationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MobileverificationPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MobileverificationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

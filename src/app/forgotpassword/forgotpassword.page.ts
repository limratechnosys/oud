import { Component } from '@angular/core';
import { Events, LoadingController, ToastController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Location } from '@angular/common';

import { AppSettings } from './../urlset';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: 'forgotpassword.page.html',
  styleUrls: ['forgotpassword.page.scss'],
})

export class ForgotPasswordPage {
    image_url:any=AppSettings.image_url; 
    otpSent:boolean=false;
    forgotPasswordRequestForm: FormGroup;  
    forgotPasswordForm: FormGroup;  
    forgotpassword={
        mobile:'',
        otp:'',
        newpassword:'',
       confirmpassword:''
    }
    
    constructor(private router: Router, public http:Http, formBuilder: FormBuilder, public events:Events, public loadingController: LoadingController, 
    public toastController: ToastController, private route: ActivatedRoute, private _location: Location) {
        this.forgotPasswordRequestForm = formBuilder.group({
            'mobile':['',Validators.compose([Validators.required,Validators.pattern(/^[0-9]{10}$/),Validators.maxLength(10),Validators.minLength(10)])],
        });    
        this.forgotPasswordForm = formBuilder.group({
            'otp':['',Validators.compose([Validators.required, Validators.minLength(4)])],
            'newpassword':['',Validators.compose([Validators.required, Validators.minLength(6)])],
            'confirmpassword':['',Validators.compose([Validators.required, Validators.minLength(6)])],
        });    
    }
      
     //forgot password request
    async forgot_password_request(){  
        let loading = await this.loadingController.create({
            message: 'Please wait...',
            cssClass: 'custom-loading'
        });
        loading.present();  
        var headers = new Headers();
        headers.append("Accept", 'application/json');
        headers.append('Content-Type', 'application/json' );
        const requestOptions = new RequestOptions({ headers: headers });
        
        this.http.post(AppSettings.backend_url +'Gotitusermobile/forgot_password_request', this.forgotpassword, requestOptions).subscribe(data => {
            let response = JSON.parse(data['_body']);
            loading.dismiss();
            if (response.status == true) {    
                this.showMessage('success', response.message);
                this.otpSent=true;
            }else{   
                this.showMessage('failed', response.message);  
            }
        }, error => {
            console.log(error);
        });
    }  
      
      //forgot password 
    async forgot_password(){  
        if(this.forgotpassword.newpassword == this.forgotpassword.confirmpassword){  
            let loading = await this.loadingController.create({
                message: 'Please wait...',
                cssClass: 'custom-loading'
            });
            loading.present();  
            var headers = new Headers();
            headers.append("Accept", 'application/json');
            headers.append('Content-Type', 'application/json' );
            const requestOptions = new RequestOptions({ headers: headers });

            this.http.post(AppSettings.backend_url +'Gotitusermobile/forgot_password', this.forgotpassword, requestOptions).subscribe(data => {
                let response = JSON.parse(data['_body']);
                loading.dismiss();
                if (response.status == true) {    
                    this.showMessage('success', response.message);
                    setTimeout(()=>{
                        this.router.navigateByUrl('/login/home');
                    },300);
                }else{   
                    this.showMessage('failed', response.message);  
                }
            }, error => {
                console.log(error);
            });
        }else{
                this.showMessage('failed','New Password & Confirm Password do not match, please try again.');          
        }
    }  
    
     // show message into toast
    async showMessage(type, message) {
        if(type=='success'){
            let successtoast = await this.toastController.create({
                message: message,
                duration: 3000,
                cssClass:'success-toast'
            });
            successtoast.present();
        }else{
             let failedtoast = await this.toastController.create({
                message: message,
                duration: 3000,
                cssClass:'failed-toast'
            });
            failedtoast.present();
        }
    }
    
    //close current Page
    closeCurrentPage(){
        this._location.back(); 
    }
}

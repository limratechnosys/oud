import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCollectablePage } from './add-collectable.page';

describe('AddCollectablePage', () => {
  let component: AddCollectablePage;
  let fixture: ComponentFixture<AddCollectablePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCollectablePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCollectablePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { Events, ToastController, ActionSheetController, LoadingController, AlertController,ModalController } from '@ionic/angular';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { AppSettings } from './../urlset';
import { ApiService } from '../service/api.service';
import { Http, Response, RequestOptions, Headers} from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';

import { Location } from '@angular/common';


@Component({
  selector: 'app-add-collectable',
  templateUrl: './add-collectable.page.html',
  styleUrls: ['./add-collectable.page.scss'],
})
export class AddCollectablePage implements OnInit {
	image_url:any=AppSettings.image_url;
	collectForm: FormGroup;
    arrcollect={
        category_id:0,
        sub_category_id:0,
        collectable_name:'',
        location:'',
        price:'',
        market_value:'',
        quantity:'',
        description:'',
        dynamicfields:'',
        photos:''
    }
    arrcategory:any=[];
    arrsubcategory:any=[];
	constructor(
		public actionSheetController: ActionSheetController,
		public loadingController: LoadingController,
		public toastController: ToastController,
		public formBuilder: FormBuilder,
		public events:Events,
		public api:ApiService,
		private router: Router,
        public alertController: AlertController,
        private _location: Location) { 

		this.collectForm = formBuilder.group({
            'category_id':['',Validators.compose([Validators.required])],
            'sub_category_id':['',Validators.compose([Validators.required])]
        });
		this.getCategory();
        
	}
    //when this will load
    ionViewWillEnter(){
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if(currentUser === 'undefined' || currentUser === null){
            this.router.navigateByUrl('/login/home');
        }else{
            let check = JSON.parse(localStorage.getItem('arrcollect'));
            if(check !== 'undefined' || check !== null){
                let arr = JSON.parse(localStorage.getItem('arrcollect'));
                console.log("arrcollect",this.arrcollect);
                setTimeout(()=>{
                    this.arrcollect.category_id=arr.category_id;
                    this.collectForm.controls['category_id'].setValue(this.arrcollect.category_id);
                    setTimeout(()=>{
                        this.arrcollect.sub_category_id=arr.sub_category_id;
                        this.collectForm.controls['sub_category_id'].setValue(this.arrcollect.sub_category_id);

                    },700);  
                },400);

            }
        }
    }

	//Get category
	async getCategory(){
        let loading = await this.loadingController.create({
            message: 'Please wait...',
            cssClass: 'custom-loading'
        });
        loading.present();
        this.api.postApi('get_categories',{}).then(res => {
            let response : any = res;
            loading.dismiss();
            if (response.status == true) {
            	this.arrcategory = response.categories;
            }
        });	 
	}

	//Get sub category
	async getsubCategory(){
        if(this.arrcollect.category_id==9999){
            const alert = await this.alertController.create({
              header: 'Enter',
                inputs: [
                {
                  name: 'category',
                  type: 'text',
                  placeholder: 'Category*'
                },
                {
                  name: 'sub_category',
                  type: 'text',
                  placeholder: 'Sub Category*'
                }],
                buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (blah) => {}
                },
                {
                  text: 'Ok',
                  handler: (data) => {
                    if(data.category==""){
                      this.showMessage('failed',"Please enter category.");
                      return false;
                    }
                    else if(data.sub_category==""){
                      this.showMessage('failed',"Please enter sub category.");
                      return false;
                    }else{
                        this.addcategoryDetails(data);
                    }
                  }
                }
              ]
            });
            await alert.present();
        }else{
            let loading = await this.loadingController.create({
                message: 'Please wait...',
                cssClass: 'custom-loading'
            });
            loading.present();
            this.api.postApi('get_sub_categories',{id:this.arrcollect.category_id}).then(res => {
                let response : any = res;
                loading.dismiss();
                if (response.status == true) {
                	this.arrsubcategory = response.subcategories;
                    this.updatedata();
                }
            });	
        }
	}

    async addcategoryDetails(data){
        let loading = await this.loadingController.create({
            message: 'Please wait...',
            cssClass: 'custom-loading'
        });
        loading.present();
        this.api.postApi('addcategoryDetails',data).then(res => {
            let response : any = res;

            loading.dismiss();
            if (response.status == true) {
                console.log("response",response);
                let arrdata = response.data;
                setTimeout(()=>{
                    this.arrcategory.push({
                        id:arrdata.category_id,
                        name:data.category,
                    });

                    this.arrsubcategory=[];
                    this.arrsubcategory.push({
                        id:arrdata.sub_category_id,
                        name:data.sub_category,
                    });
                    setTimeout(()=>{
                        this.arrcollect.category_id=arrdata.category_id;
                        this.arrcollect.sub_category_id=arrdata.sub_category_id;
                        console.log(this.arrcollect)
                        this.collectForm.controls['category_id'].setValue(this.arrcollect.category_id);
                        this.collectForm.controls['sub_category_id'].setValue(this.arrcollect.sub_category_id);
                    },400);
                },700);
               
            }else{
                this.showMessage('failed',response.message);
                return false;
            }
        });    
    }

    // show message into toast
    async showMessage(type, message) {
        if(type=='success'){
            let successtoast = await this.toastController.create({
                message: message,
                duration: 3000,
                cssClass:'success-toast'
            });
            successtoast.present();
        }else{
             let failedtoast = await this.toastController.create({
                message: message,
                duration: 3000,
                cssClass:'failed-toast'
            });
            failedtoast.present();
        }
    }

    getfields(){
        this.updatedata();
    }
    updatedata(){
        var arrcollect = JSON.stringify(this.arrcollect);
        localStorage.setItem('arrcollect', arrcollect);
        let check = JSON.parse(localStorage.getItem('arrcollect'));
    }
    // Back button
    setBackButtonAction(){
    this._location.back();
    }
	ngOnInit() {
	}

}

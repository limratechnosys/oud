import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AddCollectablePage } from './add-collectable.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    FormsModule,
    HttpModule,
    RouterModule.forChild([
      {
        path: '',
        component: AddCollectablePage
      }
    ])
  ],
  declarations: [AddCollectablePage]
})
export class AddCollectablePageModule {}


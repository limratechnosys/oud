import { Component, OnInit } from '@angular/core';
import { LoadingController,Events,ToastController} from '@ionic/angular';
import { Router, ActivatedRoute } from "@angular/router";
import { ApiService } from '../service/api.service';
import { AppSettings } from './../urlset';
import { Location } from '@angular/common';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';

/*
import { ModalPost } from '../modal-post/modal-post';
import { EditProfile } from '../edit-profile/edit-profile';
import { Options } from '../options/options';
import { TaggedProfile } from '../tagged-profile/tagged-profile';
import { SavedProfile } from '../saved-profile/saved-profile';
*/

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage {
	userId:any;
	public profile_segment:string;
	image_url:any=AppSettings.image_url;
	upload_url:any=AppSettings.uploaded_image_url;
  	sliderConfig = {
		slidesPerView: 3,
		spaceBetween: 5,
		centeredSlides: false
	};
  user_details={
    id:0,
    name:'',
    image:'',
    login_user_id:0
  };

  other_details={
    followersCnt:0,
    followingCnt:0,
    postCnt:0,
    is_follow:0
  };
	arrdetails:any=[];
	allImageList:any=[];
  	constructor(
  		public loadingController: LoadingController,
  		private route: ActivatedRoute,
  		public api:ApiService,
  		private _location: Location,
      public events:Events,
      private socialSharing: SocialSharing,
      private photoViewer: PhotoViewer) { 

	  	this.route.params.subscribe( params => {
	        this.userId = params.userId;
	    }); 
  	}

  	// Define segment for everytime when profile page is active
  	ionViewWillEnter() {
	    this.getproductDetails();
  	}
  	async getproductDetails(){
		let loading = await this.loadingController.create({
		    message: 'Please wait...',
		    cssClass: 'custom-loading'
		});
		loading.present();
		this.api.postApi('getuserProfileDetails',{userId:this.userId},true).then(res => {
		    let response : any = res;
		    loading.dismiss();
		    console.log("response",response)
		    if (response.status == true) {
		    	this.arrdetails=response.productList;
          this.user_details=response.user_details;
          this.other_details=response.other_details;
          this.allImageList=response.productImages;
		    	this.profile_segment = 'All';
		    }
		}); 
	}
  
  
  //Follow to user
  async followUser(){
    let loading = await this.loadingController.create({
        message: 'Please wait...',
        cssClass: 'custom-loading'
    });
    loading.present();
    this.api.postApi('followUsers',{userId:this.userId},true).then(res => {
      let response : any = res;
      loading.dismiss();
      if (response.status == true) {
        this.other_details.is_follow=1;
        this.other_details.followersCnt+=1;
      }
    }); 
  }

  likeButton(scindex,pindex) {
    let data={ 'id': this.arrdetails[scindex].postDetails[pindex].product_id};
    this.api.postApi('add_to_wishlist',data,true).then(res => {
      let response : any = res;
      if (response.status == true) {
        this.events.publish('updatehomePagedetails');
        this.arrdetails[scindex].postDetails[pindex].is_like=1;
        this.arrdetails[scindex].postDetails[pindex].totalLikes+=1;
      }else if(response.status == false){
        if(this.arrdetails[scindex].postDetails[pindex].totalLikes>0){
          this.arrdetails[scindex].postDetails[pindex].totalLikes-=1;
          this.arrdetails[scindex].postDetails[pindex].is_like=0;
        }
      }
    });
  }

  //share product details
  share_product_details(scindex,pindex){
    let file = this.upload_url+this.arrdetails[scindex].postDetails[pindex].imageDetails[0].image;
    let productname = "Check this out "+this.arrdetails[scindex].postDetails[pindex].product_name;
    let url = this.upload_url+"ProductDetails/" + this.arrdetails[scindex].postDetails[pindex].product_id;
    this.socialSharing.share(productname, null, file, url);
  }

	// Back button
  setBackButtonAction(){
    this._location.back();
  }
  goEditProfile(){
    
  }
  // view image
  viewImage(image){
    let imagePath = this.upload_url+image;
    this.photoViewer.show(imagePath);
 }

}


import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: './home/home.module#HomePageModule'
  },{
    path: 'login/:loginFromModule',
    loadChildren: './login/login.module#LoginPageModule'
  },{
    path: 'signup',
    loadChildren: './signup/signup.module#SignUpPageModule'
  },
  {
    path: 'mobileverification/:userID',
    loadChildren: './mobileverification/mobileverification.module#MobileVerificationPageModule'
  },{
    path: 'forgotpassword',
    loadChildren: './forgotpassword/forgotpassword.module#ForgotPasswordPageModule'
  },{
    path: 'changepassword',
    loadChildren: './changepassword/changepassword.module#ChangePasswordPageModule'
  },{
    path: 'myaccount',
    loadChildren: './myaccount/myaccount.module#MyAccountPageModule'
  },{
    path: 'notifications',
    loadChildren: './notifications/notifications.module#NotificationsPageModule'
  },
  
  { path: 'my-collectable', loadChildren: './my-collectable/my-collectable.module#MyCollectablePageModule' },
  { path: 'add-collectable', loadChildren: './add-collectable/add-collectable.module#AddCollectablePageModule' },
  { path: 'timeline', loadChildren: './timeline/timeline.module#TimelinePageModule' },
  { path: 'timeline/:type', loadChildren: './timeline/timeline.module#TimelinePageModule' },
  { path: 'favorite', loadChildren: './favorite/favorite.module#FavoritePageModule' },
  { path: 'chat/:id/:name', loadChildren: './chat/chat.module#ChatPageModule' },
  { path: 'setting', loadChildren: './setting/setting.module#SettingPageModule' },
  { path: 'add-product/:id/:type', loadChildren: './add-product/add-product.module#AddProductPageModule' },
  { path: 'add-product/:id', loadChildren: './add-product/add-product.module#AddProductPageModule' },
  { path: 'add-product', loadChildren: './add-product/add-product.module#AddProductPageModule' },
  { path: 'product-details/:productId/:type', loadChildren: './product-details/product-details.module#ProductDetailsPageModule' },
  { path: 'product-details/:productId', loadChildren: './product-details/product-details.module#ProductDetailsPageModule' },
  { path: 'wishlist', loadChildren: './wishlist/wishlist.module#WishlistPageModule' },
  { path: 'chatlisting', loadChildren: './chatlisting/chatlisting.module#ChatlistingPageModule' },
  { path: 'notification', loadChildren: './notification/notification.module#NotificationPageModule' },
  { path: 'myprofile', loadChildren: './myprofile/myprofile.module#MyprofilePageModule' },
  { path: 'comments/:productId', loadChildren: './comments/comments.module#CommentsPageModule' },
  { path: 'profile/:userId', loadChildren: './profile/profile.module#ProfilePageModule' },
  { path: 'category-details/:categoryId/:categoryName', loadChildren: './category-details/category-details.module#CategoryDetailsPageModule' },
  { path: 'category-details/:categoryId/:categoryName/:productId', loadChildren: './category-details/category-details.module#CategoryDetailsPageModule' },

  { path: 'myvaluation', loadChildren: './myvaluation/myvaluation.module#MyvaluationPageModule' },
  { path: 'addcategory', loadChildren: './addcategory/addcategory.module#AddcategoryPageModule' },
  { path: 'addcategory/:id', loadChildren: './addcategory/addcategory.module#AddcategoryPageModule' },
  { path: 'subscription', loadChildren: './subscription/subscription.module#SubscriptionPageModule' },
  { path: 'paymentmessage/:paymentStatusType', loadChildren: './paymentmessage/paymentmessage.module#PaymentmessagePageModule' },
  { path: 'commentnotfications', loadChildren: './commentnotfications/commentnotfications.module#CommentnotficationsPageModule' },
  { path: 'faqs', loadChildren: './faqs/faqs.module#FaqsPageModule' },
  { path: 'followers/:userId/:type/:name', loadChildren: './followers/followers.module#FollowersPageModule' },
  { path: 'collectors', loadChildren: './collectors/collectors.module#CollectorsPageModule' },
  { path: 'review-list', loadChildren: './review-list/review-list.module#ReviewListPageModule' },
  { path: 'manage-products', loadChildren: './manage-products/manage-products.module#ManageProductsPageModule' },
  { path: 'category-list', loadChildren: './category-list/category-list.module#CategoryListPageModule' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}

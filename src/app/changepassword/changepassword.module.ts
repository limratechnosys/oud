import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';

import { ChangePasswordPage } from './changepassword.page';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    HttpModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: ChangePasswordPage
      }
    ])
  ],
  declarations: [ChangePasswordPage]
})
export class ChangePasswordPageModule {}

import { Component } from '@angular/core';
import { Events, ToastController, LoadingController } from '@ionic/angular';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Location } from '@angular/common';

import { AppSettings } from './../urlset';

@Component({
  selector: 'app-changepassword',
  templateUrl: 'changepassword.page.html',
  styleUrls: ['changepassword.page.scss']
})

export class ChangePasswordPage{
    changepasswordForm: FormGroup;  
    changepassword={
        oldpassword:'',
        newpassword:'',
        confirmpassword:''
    }
    constructor(public http:Http, formBuilder: FormBuilder, public toastController: ToastController, public loadingController: LoadingController, 
    private _location: Location, public events:Events) {
        this.events.publish('checkLoggedInToken');
        this.changepasswordForm = formBuilder.group({
            'oldpassword': ['',Validators.compose([Validators.required, Validators.minLength(6)])],
            'newpassword': ['',Validators.compose([Validators.required, Validators.minLength(6)])],
            'confirmpassword': ['',Validators.compose([Validators.required, Validators.minLength(6)])],
        });
    } 
    
    //chnage current user's password
    async change_password(){    
        if(this.changepassword.newpassword==this.changepassword.confirmpassword){    
            let loading = await this.loadingController.create({
                message: 'Please wait...',
                cssClass: 'custom-loading'
            });
            loading.present(); 

            let currentUser= JSON.parse(localStorage.getItem('currentUser'));        
            var headers = new Headers();
            headers.append("Accept", 'application/json');
            headers.append('Content-Type', 'application/json' );
            headers.append('Authorization', 'Bearer ' + currentUser.token);
            const requestOptions = new RequestOptions({ headers: headers });

            this.http.post(AppSettings.backend_url +'Gotitusermobile/change_password', this.changepassword, requestOptions).subscribe(data => {
                loading.dismiss();
                let response = JSON.parse(data['_body']);
                if (response.status == true) {      
                    this.showMessage('success', response.message);  
                    this._location.back();
                }else{
                    this.showMessage('failed', response.message);  
                }
            }, error => {
                console.log(error);
            });
        }else{
                this.showMessage('failed','New Password & Confirm Password do not match, please try again.');          
        }
    }  
    
      // show message into toast
    async showMessage(type, message) {
        if(type=='success'){
            let successtoast = await this.toastController.create({
                message: message,
                duration: 3000,
                cssClass:'success-toast'
            });
            successtoast.present();
        }else{
             let failedtoast = await this.toastController.create({
                message: message,
                duration: 3000,
                cssClass:'failed-toast'
            });
            failedtoast.present();
        }
    }
}

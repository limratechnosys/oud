import { Component, OnInit } from '@angular/core';
import { LoadingController,Events,ToastController,AlertController} from '@ionic/angular';
import { Router, ActivatedRoute } from "@angular/router";

import { AppSettings } from './../urlset';
import { ApiService } from '../service/api.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-followers',
  templateUrl: './followers.page.html',
  styleUrls: ['./followers.page.scss'],
})
export class FollowersPage {
	public profile_segment:string='Followers';
	image_url:any=AppSettings.image_url;
	upload_url:any=AppSettings.uploaded_image_url;
	userId:number=0;
	login_user_id:number=0;
	userName:any="";
	follow_details ={
		following:[],
		followers:[]
	}
  	constructor(
  		public api:ApiService,
  		public loadingController: LoadingController,
  		private _location: Location,
  		private route: ActivatedRoute,
  		public alertController: AlertController
  	) {
  		this.route.params.subscribe( params => {
	        this.userId = params.userId;
	        this.profile_segment=params.type;
	        this.userName=params.name;
	        this.getfollowerslist();
	    }); 
  	}
  	async getfollowerslist(){
	  	let loading = await this.loadingController.create({
	    message: 'Please wait...',
	    cssClass: 'custom-loading'
		});
		loading.present();
		this.api.postApi('getfollowerslist',{userId:this.userId},true).then(res => {
		    let response : any = res;
		    loading.dismiss();
		    if (response.status == true) {
		    	console.log("response",response);
		    	this.follow_details=response.data;
		    	this.login_user_id=response.login_user_id;
		    	
		    }
		}); 
	}
	followuser(icnt){
		if(!this.follow_details.followers[icnt].is_follow){
			this.follow_details.followers[icnt].is_follow=1;
			this.api.postApi('followUsers',{userId:this.follow_details.followers[icnt].id},true).then(res => {
		      let response : any = res;
		      if (response.status == true) {
		        console.log("response",response)
		      }
		    }); 
		}
	}
	unfollow_user(icnt){
		let userId=this.follow_details.followers[icnt].id;
		this.follow_details.followers.splice(icnt, 1);
		this.api.postApi('unfollow_user',{userId:userId},true).then(res => {
	      let response : any = res;
	      if (response.status == true) {
	      }
	    }); 
	}
	unfollowing(icnt){
		let userId=this.follow_details.following[icnt].id;
		this.follow_details.following.splice(icnt, 1);
		this.api.postApi('unfollowing_user',{userId:userId},true).then(res => {
	      let response : any = res;
	      if (response.status == true) {
	      }
	    }); 
	}
	async unfollowing_user(icnt){
		const alert = await this.alertController.create({
            header: 'Unfollow',
            message: 'Are you sure?',
            buttons: [
              {
                text: 'Unfollow',
                cssClass: 'btn-yes',
                handler: () => {
                    this.unfollowing(icnt);
                }
              },
              {
                text: 'Cancel',
                role: 'cancel',
                cssClass: 'btn-no',
                handler: (blah) => {}
              }
            ]
        });
        await alert.present();

		
	}

	// Back button
    setBackButtonAction(){
      this._location.back();
    }
}

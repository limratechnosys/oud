import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CollectorsPage } from './collectors.page';

describe('CollectorsPage', () => {
  let component: CollectorsPage;
  let fixture: ComponentFixture<CollectorsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CollectorsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollectorsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

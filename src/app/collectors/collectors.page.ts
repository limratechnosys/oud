import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { LoadingController} from '@ionic/angular';
import { AppSettings } from './../urlset';
import { ApiService } from '../service/api.service';
@Component({
  selector: 'app-collectors',
  templateUrl: './collectors.page.html',
  styleUrls: ['./collectors.page.scss'],
})
export class CollectorsPage{
	arrcategory:any=[];
	arrcollectores:any=[];
	image_url:any=AppSettings.image_url;
    upload_url:any=AppSettings.uploaded_image_url;
    category_id:number=0;
	constructor(
		private _location: Location,
		public loadingController: LoadingController,
		public api:ApiService) { 
		this.getCategory();
	}

  // Back button
	setBackButtonAction(){
	  this._location.back();
	}
	//Get category
    async getCategory(){
        let loading = await this.loadingController.create({
            message: 'Please wait...',
            cssClass: 'custom-loading'
        });
        loading.present();
        this.api.postApi('get_categories',{},false).then(res => {
            let response : any = res;
            loading.dismiss();
            if (response.status == true) {
                this.arrcategory = response.categories;
            }
        });     
    }
    async getcollectores(){
    	let loading = await this.loadingController.create({
            message: 'Please wait...',
            cssClass: 'custom-loading'
        });
        loading.present();
        this.api.postApi('getcollectors',{category_id:this.category_id},true).then(res => {
            let response : any = res;
            loading.dismiss();
            console.log("arrcollectores response",response);
            if (response.status == true) {
                this.arrcollectores = response.data;
                
            }
        });     
    }
}

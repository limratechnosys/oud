
import { Component } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { Location } from '@angular/common';

import { AppSettings } from './../urlset';

@Component({
  selector: 'app-paymentmessage',
  templateUrl: 'paymentmessage.page.html',
  styleUrls: ['paymentmessage.page.scss'],
})

export class PaymentmessagePage {
    image_url:any=AppSettings.image_url;   
    paymentStatusType:any;
    
     constructor(private route: ActivatedRoute, private _location: Location) {  
        this.route.params.subscribe( params => {
            this.paymentStatusType=params.paymentStatusType;
        });       

        if(this.paymentStatusType==1){
           localStorage.removeItem('cart');
        }
    } 
    setBackButtonAction(){
    this._location.back();
  }
}

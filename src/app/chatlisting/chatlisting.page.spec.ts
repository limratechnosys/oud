import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatlistingPage } from './chatlisting.page';

describe('ChatlistingPage', () => {
  let component: ChatlistingPage;
  let fixture: ComponentFixture<ChatlistingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChatlistingPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatlistingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

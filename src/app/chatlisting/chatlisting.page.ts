import { Component, OnInit } from '@angular/core';
import { AppSettings } from './../urlset';
import { ApiService } from '../service/api.service';
import { LoadingController,Events,ToastController} from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-chatlisting',
  templateUrl: './chatlisting.page.html',
  styleUrls: ['./chatlisting.page.scss'],
})
export class ChatlistingPage {
	public chatData:Array<any>;
	public permantData:Array<any>;
	image_url:any=AppSettings.image_url;
	upload_url:any=AppSettings.uploaded_image_url;
	searchuser:any='';
  	constructor(
  		public api:ApiService,
  		public loadingController: LoadingController,
  		private router: Router,
  		public events:Events,
  		private _location: Location
  		) {

  	}

  	async getchatinguserList(){
	  	let loading = await this.loadingController.create({
	    message: 'Please wait...',
	    cssClass: 'custom-loading'
		});
		loading.present();
		this.api.postApi('getchatinguserList',{},true).then(res => {
		    let response : any = res;
		    loading.dismiss();
		    if (response.status == true) {
		    	console.log("response",response)
		    	this.chatData=response.chattingdata;
		    	this.permantData=response.users;	
		    }
		}); 
	}
	setFilteredItems(e){
		this.chatData = this.permantData.filter(item => {
			let str = item.name;
			let strvalue = str.toLowerCase();
	      	return strvalue.indexOf(e.toLowerCase()) > -1;
	    });
	}
	ionViewWillEnter(){
      let currentUser = JSON.parse(localStorage.getItem('currentUser'));
      if(currentUser === 'undefined' || currentUser === null){
          this.router.navigateByUrl('/login/home');
      }else{
          this.getchatinguserList();
      }  
  	}
  	chatDetails(id){
  		this.router.navigateByUrl('/chat/'+this.chatData[id].userId+'/'+this.chatData[id].name);
  	}
  	 // Back button
    setBackButtonAction(){
      this._location.back();
    }
}

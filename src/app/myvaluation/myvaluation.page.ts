import { Component,ViewChild } from '@angular/core';
import { AppSettings } from './../urlset';
import { ApiService } from '../service/api.service';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Events} from '@ionic/angular';
import { Router, ActivatedRoute } from "@angular/router";
import { Location } from '@angular/common';


@Component({
  selector: 'app-myvaluation',
  templateUrl: './myvaluation.page.html',
  styleUrls: ['./myvaluation.page.scss'],
})
export class MyvaluationPage {
	totalValue={
		marketValues:[],
		perchaseValues:[]
	}
  	constructor( public api:ApiService,
		public sanitizer: DomSanitizer,
		public events:Events,
    private _location: Location,
		private router: Router) {
		this.gethomepageDetails();
	}

  gethomepageDetails(){
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    let token=false;
    console.log("currentUser",currentUser);
    if(currentUser !== 'undefined' && currentUser !== null){
      token=true;
    }
    //alert(token)
    this.api.postApi('getvaluesDetails',{token:token},token).then(res => {
      let response : any = res;
      if(response.status){
        console.log("response",response);
        this.totalValue= response.data;
      }
    });
  }
  hideShowmarketvalue(icnt){
    if(this.totalValue.marketValues[icnt].is_show==1){
      this.totalValue.marketValues[icnt].is_show=0;
      this.totalValue.marketValues[icnt].icon='arrow-dropright-circle';
      
    }
    else{
      this.totalValue.marketValues[icnt].is_show=1;
      this.totalValue.marketValues[icnt].icon='arrow-dropdown-circle';
    }
  }
  hideShowperchaseValues(icnt){
    if(this.totalValue.perchaseValues[icnt].is_show==1){
      this.totalValue.perchaseValues[icnt].is_show=0;
      this.totalValue.perchaseValues[icnt].icon='arrow-dropright-circle';
      
    }
    else{
      this.totalValue.perchaseValues[icnt].is_show=1;
      this.totalValue.perchaseValues[icnt].icon='arrow-dropdown-circle';
    }
  }
  setBackButtonAction(){
    this._location.back();
  }
}

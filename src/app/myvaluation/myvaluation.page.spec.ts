import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyvaluationPage } from './myvaluation.page';

describe('MyvaluationPage', () => {
  let component: MyvaluationPage;
  let fixture: ComponentFixture<MyvaluationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyvaluationPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyvaluationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

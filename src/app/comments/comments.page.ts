import { Component, ElementRef, ViewChild} from '@angular/core';
import { Events, IonContent, LoadingController} from '@ionic/angular';
import { AppSettings } from './../urlset';
import { ApiService } from '../service/api.service';
import { Router, ActivatedRoute } from "@angular/router";
import { Location } from '@angular/common';


@Component({
  selector: 'app-comments',
  templateUrl: './comments.page.html',
  styleUrls: ['./comments.page.scss'],
})
export class CommentsPage {
	image_url:any=AppSettings.image_url;
	upload_url:any=AppSettings.uploaded_image_url;
	@ViewChild(IonContent) content: IonContent;
	@ViewChild('chat_input') messageInput: ElementRef;
	msgList:any= [];
	productId:any;
	editorMsg = '';
	showEmojiPicker = false;
	userId:any='';
	user = {
      id: 0,
      name: '',
      image:''
    };
    product_by:number=0;
    
    constructor(
		public api:ApiService,
		private events: Events,
	    private router: Router,
	    private route: ActivatedRoute,
	    private _location: Location,
	    public loadingController: LoadingController) {
		this.route.params.subscribe( params => {
	        this.productId = params.productId;
	        // Get user information
		    this.api.postApi('get_user_details',{},true).then(res => {
		  		let response : any = res;
		  		this.user=response.user_details;
		  		console.log("user",this.user)
		  		setTimeout(() => {
		  	    	this.getMsg();
		  	  	}, 400)
			});
	    });
  	}

	switchEmojiPicker() {
		this.showEmojiPicker = !this.showEmojiPicker;
		if (!this.showEmojiPicker) {
		  this.focus();
		} else {
		  this.setTextareaScroll();
		}
		//this.content.resize();
		this.scrollToBottom();
	}
	// Get message list
	getMsg() {
		this.api.postApi('getcomments',{productId:this.productId},true).then(res => {
			let response : any = res;
			if (response.status == true) {
				this.msgList=response.comments;
				this.product_by=response.product_by;
				this.scrollToBottom();
				console.log("response",response)
			}
		});
		this.scrollToBottom();
	}

	sendMsg() {
		if (!this.editorMsg.trim()) return;
		const id = Date.now().toString();
		let newMsg = {
			id:0,
			messageId: Date.now().toString(),
			userId: this.user.id,
			userName: this.user.name,
			userAvatar: this.user.image,
			time: 'Just Now',
			message: this.editorMsg,
			productId:this.productId,
			status: 'pending'
		};
		this.pushNewMsg(newMsg);
		this.editorMsg = '';
		if (!this.showEmojiPicker) {
		  this.focus();
		}
		this.api.postApi('savecommentDetails',newMsg,true).then(res => {
			let response : any = res;
			if (response.status == true) {
				this.events.publish('updatehomePagedetails');
				console.log("response",response);
				let index = this.getMsgIndexById(id);
				this.msgList[index].id=response.commentId;
			    if (index !== -1) {
			        this.msgList[index].status = 'success';
			    }
			}
		});
	}
	selectComment(icnt){
		if(this.user.id==this.product_by || this.msgList[icnt].userId==this.user.id){
			if(this.msgList[icnt].is_select){
				this.msgList[icnt].is_select=false;
			}else{
				this.msgList[icnt].is_select=true;
			}
		}
	}
	async deleteComment(commentId){
		let loading = await this.loadingController.create({
		    message: 'Please wait...',
		    cssClass: 'custom-loading'
		});
		loading.present();
		this.api.postApi('deleteComment',{commentId:commentId},true).then(res => {
		    let response : any = res;
		    loading.dismiss();
		    console.log("response",response);
		    if (response.status == true) {
		    	this.getMsg();
		    }
		}); 
	}

  pushNewMsg(msg) {
    this.msgList.push(msg);
    this.scrollToBottom();
  }

  getMsgIndexById(id: string) {
    return this.msgList.findIndex(e => e.messageId === id)
  }

  scrollToBottom() {
    setTimeout(() => {
      if (this.content.scrollToBottom) {
        this.content.scrollToBottom();
      }
    }, 400)
  }

  private focus() {
    if (this.messageInput && this.messageInput.nativeElement) {
      this.messageInput.nativeElement.focus();
    }
  }

  private setTextareaScroll() {
    const textarea =this.messageInput.nativeElement;
    textarea.scrollTop = textarea.scrollHeight;
  }
  onFocus() {
    this.showEmojiPicker = false;
    //this.content.resize();
    this.scrollToBottom();
  }
  // Back button
  setBackButtonAction(){
    this._location.back();
  }
}

import { Component, OnInit } from '@angular/core';
import { Events, ToastController, ActionSheetController, LoadingController, AlertController,ModalController } from '@ionic/angular';
import { ApiService } from '../service/api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AppSettings } from './../urlset';
import { Location } from '@angular/common';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-addcategory',
  templateUrl: './addcategory.page.html',
  styleUrls: ['./addcategory.page.scss'],
})
export class AddcategoryPage {
  categoryForm: FormGroup;
  arrcat={
    id:0,
    name:'',
    status:0,
    photos:'',
  }
  arrPhotos:any=[{
        path :'assets/icon/upload.png',
    }
  ];
  arrPhotosserver:any=[{
          image :'',
      }
  ];
  status:any='';
  removeImage:boolean=false;
  constructor(
    private sanitizer: DomSanitizer,
    private camera: Camera,
    public actionSheetController: ActionSheetController,
    private modalCtrl: ModalController,
    public loadingController: LoadingController,
    public api:ApiService,
    public toastController: ToastController,
    private router: Router,
    public events:Events,
    private route: ActivatedRoute,
    private _location: Location,
    public formBuilder: FormBuilder) { 
      this.categoryForm = formBuilder.group({
        'name':['',Validators.compose([Validators.required])],
        'status':['','']
      });
      this.route.params.subscribe( params => {
        if(params.id>0){
          this.arrcat.id = params.id;
          if(this.arrcat.id>0){
            this.getcategoryDetails();
          }
        }
    });
  }
  async getcategoryDetails(){
    let loading = await this.loadingController.create({
      message: 'Please wait...',
      cssClass: 'custom-loading'
    });
    loading.present();
    this.api.postApi('getcategoryDetails',{categoryId:this.arrcat.id},true).then(res => {
        let response : any = res;
        loading.dismiss();
        console.log("response",response)
        if (response.status == true) {
            this.arrcat = response.category_details
            if(this.arrcat.status==0){
              this.arrcat.status=0;
            }
            this.arrPhotos = response.category_details.image;
        }
    }); 
  }


  //Close Modal
  closemodel(){
    this.modalCtrl.dismiss({
      'dismissed': false
    });
  }

  removePhoto(i){
    this.arrPhotos[i].path='assets/icon/upload.png';
    this.arrPhotosserver.image='';
    this.removeImage=false;
  }

  //choose option for upload image
  async uploadImage(icnt) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Picture',
      buttons: [{
        text: 'Camera',
        icon: 'camera',
        handler: () => {
          this.selectImage(this.camera.PictureSourceType.CAMERA,icnt);
        }
        },
        {
            text: 'Gallery',
            icon: 'albums',
            handler: () => {
                this.selectImage(this.camera.PictureSourceType.PHOTOLIBRARY,icnt);
            }
          },
        {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  //select image from mobile
  selectImage(type,icnt){
    const options: CameraOptions = {
      quality: 100,
      targetWidth: 500,
      targetHeight: 500,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType:type,
      allowEdit: true,
      correctOrientation : true
      //saveToPhotoAlbum: true
    }
    this.camera.getPicture(options).then((imageData) => {
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      this.arrPhotos[icnt].path=this.sanitizer.bypassSecurityTrustUrl(base64Image);
      this.arrPhotosserver[icnt].image=imageData;
      this.removeImage=true;
      this.showMessage('success', "Photo uploaded successfully!");
    }, (err) => {
        //this.showMessage('failed', err);
    });
  }

   //Close Modal
  closePictureModal(){
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }
  // Back button
  setBackButtonAction(){
    this._location.back();
  }
  
  async addCategory(){
    if(this.arrcat.name==""){
      this.showMessage('error', "Please enter category name.");
      return false;
    }
    if(this.arrPhotosserver[0].image=='' && this.arrcat.id<=0){
      this.showMessage('error', "Please upload category image.");
      return false;
    }
    

    let loading = await this.loadingController.create({
        message: 'Please wait...',
        cssClass: 'custom-loading'
    });
   

    loading.present();
    console.log("cart",this.arrcat.status)
    this.status = this.arrcat.status;
    if(this.status==true || this.status==1){
      this.arrcat.status=1;
    }else{
      this.arrcat.status=0;
    }
    this.arrcat.photos=JSON.stringify(this.arrPhotosserver);
    this.api.postApi('adduserCategory',this.arrcat,true).then(res => {
        let response : any = res;
        console.log("response",response);
        loading.dismiss();
        if (response.status == true) {
          this.events.publish('updatehomePagedetails');
          this.showMessage('success',response.message);
          if(this.arrcat.id>0){
              this.router.navigateByUrl('/category-list');
          }else{
            this.closePictureModal();
          }
        }else{
          this.showMessage('error', response.message);
        }
    });   
  }
  // show message into toast
  async showMessage(type, message) {
    if(type=='success'){
        let successtoast = await this.toastController.create({
            message: message,
            duration: 3000,
            cssClass:'success-toast'
        });
        successtoast.present();
    }else{
         let failedtoast = await this.toastController.create({
            message: message,
            duration: 3000,
            cssClass:'failed-toast'
        });
        failedtoast.present();
    }
  }
}

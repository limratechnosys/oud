import { Component } from '@angular/core';
import { Events, LoadingController, ToastController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Http, Response, RequestOptions, Headers} from '@angular/http';
import { Location } from '@angular/common';
import { ApiService } from '../service/api.service';

import { AppSettings } from './../urlset';

@Component({
  selector: 'app-signup',
  templateUrl: 'signup.page.html',
  styleUrls: ['signup.page.scss'],
})

export class SignUpPage {
    image_url:any=AppSettings.image_url;
    signupForm: FormGroup;
    signup={
        name:'',
        mobile:'',
        email:'',
        location:'',
        password:'',
        confirmpassword:''
    }

    constructor(public api:ApiService,private router: Router, public http:Http, formBuilder: FormBuilder, public events:Events, public loadingController: LoadingController,
    public toastController: ToastController, private route: ActivatedRoute, private _location: Location) {
        this.signupForm = formBuilder.group({
            'name':['',Validators.compose([Validators.required])],
            'mobile':['',Validators.compose([Validators.required,Validators.pattern(/^[0-9]{10}$/),Validators.maxLength(10),Validators.minLength(10)])],
            'location':['',Validators.compose([Validators.required])],
            'email':['', Validators.compose([Validators.required,Validators.maxLength(30), Validators.pattern(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)])],
            'password':['',Validators.compose([Validators.required, Validators.minLength(6)])],
            'confirmpassword':['',Validators.compose([Validators.required, Validators.minLength(6)])]
        });
    }

     //register new user
    async doSignUp(){
        this.events.publish('internetConnection');
        let loading = await this.loadingController.create({
            message: 'Please wait...',
            cssClass: 'custom-loading'
        });
        loading.present();
        this.api.postApi('sign_up',this.signup).then(res => {
            let response : any = res;
            loading.dismiss();
            console.log("response",response)
            if (response.status == true) {
                this.showMessage('success', response.message);
                this.router.navigateByUrl('/login/home');
            }else{
                this.showMessage('failed', response.message);
            }
        });
    }

     // show message into toast
    async showMessage(type, message) {
        if(type=='success'){
            let successtoast = await this.toastController.create({
                message: message,
                duration: 3000,
                cssClass:'success-toast'
            });
            successtoast.present();
        }else{
             let failedtoast = await this.toastController.create({
                message: message,
                duration: 3000,
                cssClass:'failed-toast'
            });
            failedtoast.present();
        }
    }

    //close current Page
    closeCurrentPage(){
        this._location.back();
    }

}

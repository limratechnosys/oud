import { Component, ElementRef, ViewChild } from '@angular/core';
import { Events, IonContent } from '@ionic/angular';
import { AppSettings } from './../urlset';
import { ApiService } from '../service/api.service';
import { Router, ActivatedRoute } from "@angular/router";
import { Location } from '@angular/common';


@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})
export class ChatPage {
	image_url:any=AppSettings.image_url;
	upload_url:any=AppSettings.uploaded_image_url;
	@ViewChild(IonContent) content: IonContent;
	@ViewChild('chat_input') messageInput: ElementRef;
	msgList:any= [];
	toUser: any;
	editorMsg = '';
	showEmojiPicker = false;
	user = {
      id: 0,
      name: '',
      image:''
    };
    constructor(
		public api:ApiService,
		private events: Events,
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location
	) {
    // Get the navParams toUserId parameter
    this.route.params.subscribe( params => {
        this.toUser = {
          id: params.id,
          name: params.name,
        };
        console.log("toUser",this.toUser)
    });

    
    // Get user information
    this.api.postApi('get_user_details',{},true).then(res => {
  		let response : any = res;
  		this.user=response.user_details;
  		setTimeout(() => {
  	    this.getMsg();
  	  }, 400)
	  });
  }

  ionViewWillLeave() {
    // unsubscribe
    this.events.unsubscribe('chat:received');
  }

  ionViewDidEnter() {
    // Subscribe to received  new message events
    this.events.subscribe('chat:received', msg => {
      this.pushNewMsg(msg);
    })
  }

  onFocus() {
    this.showEmojiPicker = false;
    //this.content.resize();
    this.scrollToBottom();
  }

  switchEmojiPicker() {
    this.showEmojiPicker = !this.showEmojiPicker;
    if (!this.showEmojiPicker) {
      this.focus();
    } else {
      this.setTextareaScroll();
    }
    //this.content.resize();
    this.scrollToBottom();
  }
  // Get message list
  getMsg() {
    this.api.postApi('getchattingmsg',{to_user_id:this.toUser.id},true).then(res => {
  		let response : any = res;
  		if (response.status == true) {
  			this.msgList=response.chatings;
  			this.scrollToBottom();
  			console.log("response",response)
  		}
  	});
    this.scrollToBottom();
  }

  sendMsg() {
    if (!this.editorMsg.trim()) return;
    const id = Date.now().toString();
    let newMsg = {
      messageId: Date.now().toString(),
      userId: this.user.id,
      userName: this.user.name,
      userAvatar: this.user.image,
      toUserId: this.toUser.id,
      time: Date.now(),
      message: this.editorMsg,
      status: 'pending'
    };

    this.pushNewMsg(newMsg);
    this.editorMsg = '';

    if (!this.showEmojiPicker) {
      this.focus();
    }
    this.api.postApi('savechatingDetails',newMsg,true).then(res => {
  		let response : any = res;
  		if (response.status == true) {
  			console.log("response",response);
  			let index = this.getMsgIndexById(id);
		    if (index !== -1) {
		        this.msgList[index].status = 'success';
		    }
  		}
  	});
  }


  pushNewMsg(msg) {
    const userId = this.user.id,
      toUserId = this.toUser.id;
    // Verify user relationships
    if (msg.userId === userId && msg.toUserId === toUserId) {
      this.msgList.push(msg);
    } else if (msg.toUserId === userId && msg.userId === toUserId) {
      this.msgList.push(msg);
    }
    this.scrollToBottom();
  }

  getMsgIndexById(id: string) {
    return this.msgList.findIndex(e => e.messageId === id)
  }

  scrollToBottom() {
    setTimeout(() => {
      if (this.content.scrollToBottom) {
        this.content.scrollToBottom();
      }
    }, 400)
  }

  private focus() {
    if (this.messageInput && this.messageInput.nativeElement) {
      this.messageInput.nativeElement.focus();
    }
  }

  private setTextareaScroll() {
    const textarea =this.messageInput.nativeElement;
    textarea.scrollTop = textarea.scrollHeight;
  }

  // Back button
  setBackButtonAction(){
    this._location.back();
  }
}
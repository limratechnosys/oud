import { Component, OnInit } from '@angular/core';
import { LoadingController,Events,ToastController,AlertController,ModalController} from '@ionic/angular';
import { Router, ActivatedRoute } from "@angular/router";
import { ApiService } from '../service/api.service';
import { AppSettings } from './../urlset';
import { Location } from '@angular/common';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { AddcategoryPage } from '../addcategory/addcategory.page';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.page.html',
  styleUrls: ['./category-list.page.scss'],
})
export class CategoryListPage {
	categoryId:any;
	categoryName:any;
	image_url:any=AppSettings.image_url;
	upload_url:any=AppSettings.uploaded_image_url;
	arrdetails:any=[];
	page:number=1;   
	showLoader:boolean=true;
	constructor(
		public loadingController: LoadingController,
		private route: ActivatedRoute,
		public api:ApiService,
		private _location: Location,
		private socialSharing: SocialSharing,
		public events:Events,
		public toastController: ToastController,
		public alertController: AlertController,
	    private photoViewer: PhotoViewer,
	    public sanitizer: DomSanitizer,
	    public modalCtrl: ModalController) { 

	  	this.route.params.subscribe( params => {
	        this.categoryId = params.categoryId;	
	        this.categoryName = params.categoryName;	
	    }); 
	}

	async openCategory() {
        const myModal = await this.modalCtrl.create({
          component: AddcategoryPage,
          cssClass: 'my-custom-modal-css'
        });
        myModal.onDidDismiss().then((data) => {
            if(data.data.dismissed){
                this.getcategories();
            }
        });
        return await myModal.present();
    }
	
	// Define segment for everytime when profile page is active
	ionViewWillEnter() {
    	this.getcategories();
	}

	async getcategories(){
		this.page=1;
		let currentUser = JSON.parse(localStorage.getItem('currentUser'));
		let token=false;
		if(currentUser !== 'undefined' && currentUser !== null){
		  token=true;
		}
		let loading;
		if(this.showLoader){
		  loading = await this.loadingController.create({
		    message: 'Please wait...',
		    cssClass: 'custom-loading'
		  });
		  loading.present();
		}
		let data ={'page':this.page};
		this.api.postApi('get_users_category',data,true).then(res => {
		    let response : any = res;
		    console.log("response",response)
		    if(this.showLoader){
		      loading.dismiss();
		    }
		    if (response.status == true) {
		      this.arrdetails.categories=response.categories;
		      
		    }
		}); 
	}

 
  // Back button
    setBackButtonAction(){
      this._location.back();
    }
	
}



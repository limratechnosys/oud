import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ApiService } from '../service/api.service';
import { Events, ToastController, LoadingController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-faqs',
  templateUrl: './faqs.page.html',
  styleUrls: ['./faqs.page.scss'],
})
export class FaqsPage{
  faqsContent:any="";
  display_erros:boolean=false;
  constructor(
    public api:ApiService, 
    public loadingController: LoadingController,
    private _location: Location) {
      this.get_faqs();
    }

  ngOnInit() {
  }
  // Back button
	setBackButtonAction(){
	    this._location.back();
	}
  //get all wishlists list of current uers's
  async get_faqs(){   
    let loading = await this.loadingController.create({
    message: 'Please wait...',
    cssClass: 'custom-loading'
    });
    loading.present();
    this.api.postApi('get_faqs',{},true).then(res => {
      let response : any = res;
      console.log("response",response)
      loading.dismiss();
      if (response.status == true) {   
        this.display_erros=true;
        this.faqsContent= response.data.content
      }
    });
  }
}

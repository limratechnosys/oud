import { Component, OnInit } from '@angular/core';
import { Events,LoadingController,ToastController,AlertController} from '@ionic/angular';
import { ApiService } from '../service/api.service';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { AppSettings } from './../urlset';

import { Location } from '@angular/common';

@Component({
  selector: 'app-review-list',
  templateUrl: './review-list.page.html',
  styleUrls: ['./review-list.page.scss'],
})
export class ReviewListPage{
  review_products:any=[];
  upload_url:any=AppSettings.uploaded_image_url;
  constructor(public events:Events,public api:ApiService, private _location: Location,public loadingController: LoadingController,private socialSharing: SocialSharing) { 
  	let cart = JSON.parse(localStorage.getItem('cart'));
    if (cart!== null) {
      this.review_products=cart;
      console.log("review_products",this.review_products)
    }
  }

  	// Back button
	setBackButtonAction(){
	  this._location.back();
	}
	
	async generatePdf(){
		let loading;
		loading = await this.loadingController.create({
			message: 'Generating Pdf...',
			cssClass: 'custom-loading'
		});
		
		let cat = JSON.parse(localStorage.getItem('cart'));
		let data :any= {
			'review_products':'test',
			'cat':JSON.stringify(cat)
		}
		///data.review_products=cat;

		console.log("data",data)
		loading.present();
	    this.api.postApi('generatePdf',data,true).then(res => {
	        console.log("datadddddddddd",res);
	        let response : any = res;
	        loading.dismiss();
	        console.log("response",response)
	        if (response.status == true) {
	          	if(response.path!=''){
	          		let file = this.upload_url+response.path;
			        let productname = "Check this out";
			        this.socialSharing.share(productname, null, file,null);
			        localStorage.removeItem('cart');
                    this.events.publish('updateCartCount');
	          	}
	        }
	    }); 
	}
}

import { Component } from '@angular/core';
import { Events, ToastController, LoadingController,ActionSheetController } from '@ionic/angular';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Location } from '@angular/common';
import { ApiService } from '../service/api.service';
import { AppSettings } from './../urlset';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { DomSanitizer } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-myprofile',
  templateUrl: 'myprofile.page.html',
  styleUrls: ['myprofile.page.scss']
})
export class MyprofilePage {
    image_url:any=AppSettings.image_url;
    upload_url:any=AppSettings.uploaded_image_url;
    profileForm: FormGroup;
    profile={
        id:'',
        name:'',
        mobile:'',
        email:'',
        dob:'',
        gender:'',
        location:'',
        profile_image:''
    }
  	public image : any="";
    maxDate:any;
    constructor(public http:Http, formBuilder: FormBuilder, public toastController: ToastController, public loadingController: LoadingController,
    private _location: Location, public events:Events,public api:ApiService,private camera: Camera,public actionSheetController: ActionSheetController,private sanitizer: DomSanitizer,private router: Router) {
        this.profileForm = formBuilder.group({
            'name':['',Validators.compose([Validators.required])],
            'mobile':['',Validators.compose([Validators.required,Validators.pattern(/^[0-9]{10}$/),Validators.maxLength(10),Validators.minLength(10)])],
            'email':['', Validators.compose([Validators.required,Validators.maxLength(30), Validators.pattern(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)])],
            'dob':['',Validators.compose([Validators.required])],
            'gender':['',Validators.compose([Validators.required])],
            'location':['','']
        });
    }
    //get current user's details
    async get_user_details(){
        let loading = await this.loadingController.create({
            message: 'Please wait...',
            cssClass: 'custom-loading'
        });
        loading.present();
        this.api.postApi('get_user_details',{},true).then(res => {
            let response : any = res;
            console.log("response",response)
            loading.dismiss();
            if (response.status == true) {
                this.profile=response.user_details;
                this.image=this.upload_url+response.user_details.image;
            }																			
        }); 
    }

    //chnage current user's profile details
    async change_profile_details(){
        if(this.profile.dob>this.maxDate){
            this.showMessage('failed','Please do not select future date for date of birth.');
        }else{
          this.events.publish('internetConnection');
            let loading = await this.loadingController.create({
                message: 'Please wait...',
                cssClass: 'custom-loading'
            });
            loading.present();
            this.api.postApi('change_profile_details',this.profile,true).then(res => {
	            let response : any = res;
	            console.log("response",response)
	            loading.dismiss();
	            if (response.status == true) {
                    this.showMessage('success', response.message);
                }else{
                    this.showMessage('failed', response.message);
                }
	        });    
        }
    }
    //choose option for upload image
    async uploadImage() {
        const actionSheet = await this.actionSheetController.create({
          header: 'Picture',
          buttons: [{
            text: 'Camera',
            icon: 'camera',
            handler: () => {
              this.selectImage(this.camera.PictureSourceType.CAMERA);
            }
            },
            {
                text: 'Gallery',
                icon: 'albums',
                handler: () => {
                    this.selectImage(this.camera.PictureSourceType.PHOTOLIBRARY);
                }
              },
            {
            text: 'Cancel',
            icon: 'close',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          }]
        });
        await actionSheet.present();
    }

    //select image from mobile
     selectImage(type){
        const options: CameraOptions = {
            quality: 100,
            targetWidth: 300,
      		targetHeight: 300,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            sourceType:type,
            //saveToPhotoAlbum: true
        }
		this.camera.getPicture(options).then((imageData) => {
            console.log("imageData",imageData);
            let base64Image = 'data:image/jpeg;base64,' + imageData;
            let path : any = this.sanitizer.bypassSecurityTrustUrl(base64Image);
            this.image = base64Image;
            this.profile.profile_image=imageData; 
		}, (err) => {
		    //this.showMessage('failed', err);
		});
    }


      // show message into toast
    async showMessage(type, message) {
        if(type=='success'){
            let successtoast = await this.toastController.create({
                message: message,
                duration: 3000,
                cssClass:'success-toast'
            });
            successtoast.present();
        }else{
             let failedtoast = await this.toastController.create({
                message: message,
                duration: 3000,
                cssClass:'failed-toast'
            });
            failedtoast.present();
        }
    }
    // Back button
    setBackButtonAction(){
      this._location.back();
    }
    ionViewWillEnter(){
          let currentUser = JSON.parse(localStorage.getItem('currentUser'));
          if(currentUser === 'undefined' || currentUser === null){
              this.router.navigateByUrl('/login/home');
          }else{
              this.events.publish('checkLoggedInToken');
              this.get_user_details();
          }  
    }
}
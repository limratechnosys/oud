import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { MyprofilePage } from './myprofile.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    FormsModule,
    HttpModule,
    RouterModule.forChild([
      {
        path: '',
        component: MyprofilePage
      }
    ])
  ],
  declarations: [MyprofilePage]
})
export class MyprofilePageModule {}



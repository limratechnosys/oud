import { Component, OnInit } from '@angular/core';
import { LoadingController,Events,ToastController,AlertController} from '@ionic/angular';
import { Router, ActivatedRoute } from "@angular/router";
import { ApiService } from '../service/api.service';
import { AppSettings } from './../urlset';
import { Location } from '@angular/common';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-manage-products',
  templateUrl: './manage-products.page.html',
  styleUrls: ['./manage-products.page.scss'],
})
export class ManageProductsPage {
	categoryId:any;
	categoryName:any;
	image_url:any=AppSettings.image_url;
	upload_url:any=AppSettings.uploaded_image_url;
	arrdetails:any=[];
	review_products:any=[];
  page:number=1;   
  showLoader:boolean=true;
  searchdata:any="";
  arrcategory:any=[];
  category_id:number;
	constructor(
		public loadingController: LoadingController,
		private route: ActivatedRoute,
		public api:ApiService,
		private _location: Location,
		private socialSharing: SocialSharing,
		public events:Events,
		public toastController: ToastController,
		public alertController: AlertController,
    private photoViewer: PhotoViewer,
    public sanitizer: DomSanitizer) { 

  	this.route.params.subscribe( params => {
        this.categoryId = params.categoryId;	
        this.categoryName = params.categoryName;	
    }); 
    this.getCategory();
	}

	// Define segment for everytime when profile page is active
	ionViewWillEnter() {
    this.getproducts();
	}
  searchProducts(){
    this.showLoader=false;
    this.getproducts();
  }
	
  share(item){
    let file = this.upload_url+item.images[0].images;
    let url = "https://play.google.com/store/apps/details?id=com.oud.limra";
    let productname = "Check this out "+item.name;
    this.socialSharing.share(productname, null, file, url);
  }
  //Get category
  async getCategory(){
      let loading = await this.loadingController.create({
          message: 'Please wait...',
          cssClass: 'custom-loading'
      });
      loading.present();
      this.api.postApi('get_categories',{},true).then(res => {
          let response : any = res;
          loading.dismiss();
          if (response.status == true) {
              this.arrcategory = response.categories;
          }
      });     
  }
  
  async getproducts(){
    console.log("searchdata",this.searchdata)
    this.page=1;
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    let token=false;
    if(currentUser !== 'undefined' && currentUser !== null){
      token=true;
    }
    let loading;
    if(this.showLoader){
      loading = await this.loadingController.create({
        message: 'Please wait...',
        cssClass: 'custom-loading'
      });
      loading.present();
    }
    let data ={'page':this.page,'categoryId':this.categoryId,'type':'self','searchdata':this.searchdata,'search_cat_id':this.category_id};
    this.api.postApi('get_products',data,true).then(res => {
        let response : any = res;
        console.log("respodddnse",response)
        if(this.showLoader){
          loading.dismiss();
        }
        if (response.status == true) {
          this.arrdetails.products=response.products;
          
        }
    }); 
  }

  async deleteProduct(pid){
    const alert = await this.alertController.create({
            header: 'Delete!',
            message: 'Are you sure?',
            buttons: [
              {
                text: 'Yes',
                cssClass: 'btn-yes',
                handler: () => {
                    this.deleteprod(pid);
                }
              },
              {
                text: 'Cancel',
                role: 'cancel',
                cssClass: 'btn-no',
                handler: (blah) => {}
              }
            ]
        });
        await alert.present();

    
  }

  async deleteprod(pid){
    let loading;
      loading = await this.loadingController.create({
        message: 'Please wait...',
        cssClass: 'custom-loading'
      });
      loading.present();
    
    let data ={'pid':pid};
    this.api.postApi('deleteproduct',data,true).then(res => {
        let response : any = res;
        console.log("respodddnse",response)
        loading.dismiss();
    }); 
  }
 
  // Back button
    setBackButtonAction(){
      this._location.back();
    }
	
}



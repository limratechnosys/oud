import { Component } from '@angular/core';
import { Events, LoadingController, ToastController } from '@ionic/angular';
import { Location } from '@angular/common';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { AppSettings } from './../urlset';
import { ApiService } from '../service/api.service';

@Component({
  selector: 'app-login',
  templateUrl: 'login.page.html',
  styleUrls: ['login.page.scss'],
})

export class LoginPage {
    image_url:any=AppSettings.image_url;
    loginForm: FormGroup;
    login={
        email:'',
        password:''
    }
    loginFromModule:any;
    

    constructor(public api:ApiService,public http:Http, formBuilder: FormBuilder, public events:Events, public loadingController: LoadingController,
    public toastController: ToastController, private _location: Location, private router: Router, private route: ActivatedRoute) {
        this.loginForm = formBuilder.group({
            'email':['', Validators.compose([Validators.required,Validators.maxLength(30), Validators.pattern(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)])],
            'password': ['',Validators.compose([Validators.required, Validators.minLength(6)])]
        });
         this.route.params.subscribe( params => {
            this.loginFromModule=params.loginFromModule;
        });
    }

    //do login
    async doLogin(){
       let loading = await this.loadingController.create({
            message: 'Please wait...',
            cssClass: 'custom-loading'
        });
        loading.present();
        this.api.postApi('login',this.login).then(res => {
            let response : any = res;
            loading.dismiss();
            if (response.status == true) {
                console.log("response",response);
                this.showMessage('success', response.message);
                localStorage.setItem('currentUser', JSON.stringify({ token: response.token }));
                localStorage.setItem('user_details', JSON.stringify(response.user_details));
                this.events.publish('checkLoggedInToken');   
                this.router.navigateByUrl('/home');
                /*this._location.back();
                setTimeout(()=>{
                    this.events.publish('checkLoggedInToken');
                    //+this.loginFromModule
                    let newUrl = '/home';
                    if(localStorage.getItem('orderExist')=='1'){
                        let parcel = JSON.parse(localStorage.getItem('parcel'));
                        if(parcel !== 'undefined' && parcel !== null){
                          newUrl = '/parceldetails';
                          localStorage.setItem('orderExist','')
                        }
                    }
                   // this.router.navigateByUrl(newUrl);
                },500);*/
            }else{
                this.showMessage('failed', response.message);
            }
        });
    }

     // show message into toast
    async showMessage(type, message) {
        if(type=='success'){
            let successtoast = await this.toastController.create({
                message: message,
                duration: 3000,
                cssClass:'success-toast'
            });
            successtoast.present();
        }else{
             let failedtoast = await this.toastController.create({
                message: message,
                duration: 3000,
                cssClass:'failed-toast'
            });
            failedtoast.present();
        }
    }

}

import { Component, OnInit } from '@angular/core';
import { LoadingController,Events,ToastController,AlertController} from '@ionic/angular';
import { Router, ActivatedRoute } from "@angular/router";
import { ApiService } from '../service/api.service';
import { AppSettings } from './../urlset';
import { Location } from '@angular/common';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-category-details',
  templateUrl: './category-details.page.html',
  styleUrls: ['./category-details.page.scss'],
})
export class CategoryDetailsPage {
	categoryId:any;
	categoryName:any;
  image_url:any=AppSettings.image_url;
	upload_url:any=AppSettings.uploaded_image_url;
	arrdetails:any=[];
	review_products:any=[];
  page:number=1;   
  showLoader:boolean=true;
	constructor(
		public loadingController: LoadingController,
		private route: ActivatedRoute,
		public api:ApiService,
		private _location: Location,
		private socialSharing: SocialSharing,
		public events:Events,
		public toastController: ToastController,
		public alertController: AlertController,
    private photoViewer: PhotoViewer,
    public sanitizer: DomSanitizer) { 

  	this.route.params.subscribe( params => {
        this.categoryId = params.categoryId;	
        this.categoryName = params.categoryName;	
    }); 
	}

	// Define segment for everytime when profile page is active
	ionViewWillEnter() {
    this.getproducts();
	}
	
  share(item){
    let file = this.upload_url+item.images[0].images;
    let url = "https://play.google.com/store/apps/details?id=com.oud.limra";
    let productname = "Check this out "+item.name;
    this.socialSharing.share(productname, null, file, url);
  }
  
  async getproducts(){
    this.page=1;
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    let token=false;
    if(currentUser !== 'undefined' && currentUser !== null){
      token=true;
    }
    let loading;
    if(this.showLoader){
      loading = await this.loadingController.create({
        message: 'Please wait...',
        cssClass: 'custom-loading'
      });
      loading.present();
    }
    let data ={'page':this.page,'categoryId':this.categoryId};
    this.api.postApi('get_products',data,true).then(res => {
        console.log("datadddddddddd",res);
        let response : any = res;
        if(this.showLoader){
          loading.dismiss();
        }
        console.log("response",response)
        if (response.status == true) {
          this.arrdetails.categories=response.categories;
          this.arrdetails.products=response.products;
          this.updateCart();
        }
    }); 
  }

  //update cart details on local storage
  updateCart() {
    let cart = JSON.parse(localStorage.getItem('cart'));
    if (cart!== null) {
      this.review_products=cart;
    }else{
      this.review_products=[];
    }
    
    for(let cartProduct of this.review_products) {
      let index = this.arrdetails.products.findIndex((item) => item.id === cartProduct.id);
      if(index>=0){
        this.arrdetails.products[index].qty=cartProduct.qty;
      }
    }
  };

  updateQty(events,details){
    let id=details.id;
    if(events>0){
      let index = this.review_products.findIndex((item) => item.id === id);
      if(index>=0){
        this.review_products[index]={
          id:id,
          name:details.name,
          model_no:details.model_no,
          qty:events,
          image:this.upload_url+details.images[0].images
        };
      }else{
        this.review_products.push({
          id:id,
          name:details.name,
          model_no:details.model_no,
          qty:events,
          image:this.upload_url+details.images[0].images
        });
      }
    }else{
      let index = this.review_products.findIndex((item) => item.id === id);
      if(index>=0){
        this.review_products.splice(index, 1);
      }
    }
    var cart = JSON.stringify(this.review_products);
    localStorage.setItem('cart', cart);
    setTimeout(()=>{
      this.events.publish('updateCartCount');
    },500);

  }
  // Back button
    setBackButtonAction(){
      this._location.back();
    }
	
}



import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers, URLSearchParams} from '@angular/http';
import { AppSettings } from './../urlset';
import { Events} from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  // API path
  baseUrl = AppSettings.backend_url;
  constructor(public http:Http,public events:Events) { }

// Sending a GET request to /data
public getApi(path,token=false): any{
	console.log(this.baseUrl + path);
	this.events.publish('internetConnection');
	let headers = new Headers();
	headers.append("Accept", 'application/json');
	headers.append('Content-Type', 'application/json' );
	if(token){
		let currentUser= JSON.parse(localStorage.getItem('currentUser'));  
		if(currentUser!== 'undefined' && currentUser !== null){
			headers.append('Authorization', 'Bearer ' + currentUser.token);
		}
	}
	let options = new RequestOptions({headers: headers});
	return new Promise((resolve,reject) => {
	  	return  this.http.get(this.baseUrl + path,options).subscribe(data => {
	      	data = JSON.parse(data['_body']);
	        resolve(data);
	    }, error => {
	        reject(error);
	        console.log(error);// Error getting the data
	    });
	});
}

// Sending a POST request to /data
	public postApi(path, postData,token=false) {
		this.events.publish('internetConnection');
	    let headers = new Headers();
        headers.append("Accept", 'application/json');
    	headers.append('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8;');

	    if(token){
	    	let currentUser= JSON.parse(localStorage.getItem('currentUser')); 
	    	if(currentUser!== 'undefined' && currentUser !== null){    
	    		headers.append('Authorization', 'Bearer ' + currentUser.token);
	    	}
	    }
	    let options = new RequestOptions({headers: headers});
	    let postParams = postData;
	    let params = new URLSearchParams();
	    for (let key in postParams) {
	      params.set(key, postParams[key])
	    }
	    console.log("postData",postData);
	    let server_url = this.baseUrl + path;
	    console.log("server_url",server_url);
	    return new Promise((resolve,reject) => {
	      this.http.post(server_url, params, options)
	        .subscribe(data => {
	          data = JSON.parse(data['_body']);
	          resolve(data);
	        }, error => {
	          console.log(error);// Error getting the data
	          reject(error);
	          //this.alertProvider.showToast("Server Error");
	        });
	    });
  	}
}
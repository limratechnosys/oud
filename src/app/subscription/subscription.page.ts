import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { AppSettings } from './../urlset';
import { ApiService } from '../service/api.service';
import { Events,LoadingController,ToastController} from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-subscription',
  templateUrl: './subscription.page.html',
  styleUrls: ['./subscription.page.scss'],
})
export class SubscriptionPage{
	image_url:any=AppSettings.image_url;
  	upload_url:any=AppSettings.uploaded_image_url;
	sliderConfig = {
	    slidesPerView: 1.3,
	    spaceBetween: 1,
	    centeredSlides: false
	};
	plans:any=[];
	browser:any;
	sliders:any=[{
		image:'banner/subscription_banner.png'
	}];
	clearObservable:any;
	showBanner:boolean=false;
	constructor(
		private _location: Location,
		public api:ApiService,  	
		public loadingController: LoadingController,
		private iab: InAppBrowser,
		private route: ActivatedRoute,
		private router: Router,
		public toastController: ToastController) { 
		this.getPlanList();
	}

	async getPlanList(){
	    let loading = await this.loadingController.create({
		    message: 'Please wait...',
		    cssClass: 'custom-loading'
		});
		loading.present();
		this.api.postApi('getPlanList',{},true).then(res => {
		    let response : any = res;
		    loading.dismiss();
		    console.log("response",response)
		    if (response.status == true) {
		    	this.plans=response.plans;
		    	this.showBanner=true;

		    }
		}); 
	}

	// Back button
	setBackButtonAction(){
		this._location.back();
	}
	async buynow(icnt){
		let loading = await this.loadingController.create({
		    message: 'Please wait...',
		    cssClass: 'custom-loading'
		});
		loading.present();
		var data={data:JSON.stringify(this.plans[icnt])};
		this.api.postApi('place_order',data,true).then(res => {
		    let response : any = res;
		    loading.dismiss();
		    console.log("response",response)
		    if(response.url){
				this.browser = this.iab.create(response.url,'_blank','location=yes, hardwareback=no');
		        this.clearObservable = setInterval(()=> {
		            this.check_order_payment(response.order_id);
		        },3000);
		    }
	    });
	}
	check_order_payment(order_id){
		let data = { 'order_id':order_id };
		this.api.postApi('check_order_payment',data,true).then(res => {
		    let response : any = res;
		    if (response.status == true) {
                clearInterval(this.clearObservable);
                this.browser.close();
                //this.showMessage('success','Plan purchased successfully.');
                this.router.navigateByUrl('/paymentmessage/1');
            }else if (response.status == false) {
                //clear observation and close payment page
                clearInterval(this.clearObservable);
                this.browser.close();
                // remove parcel Item from localstorage
                this.router.navigateByUrl('/paymentmessage/2');
                //this.showMessage('success','Sorry, your transaction has been declined. Please try again.');
            }
		});
	}
	// show message into toast
	async showMessage(type, message) {
		if(type=='success'){
		    let successtoast = await this.toastController.create({
		        message: message,
		        duration: 3000,
		        cssClass:'success-toast'
		    });
		    successtoast.present();
		}else{
		     let failedtoast = await this.toastController.create({
		        message: message,
		        duration: 3000,
		        cssClass:'failed-toast'
		    });
		    failedtoast.present();
		}
	}
}

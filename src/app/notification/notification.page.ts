import { Component } from '@angular/core';
import { Events, ToastController, LoadingController, AlertController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Location } from '@angular/common';
import { ApiService } from '../service/api.service';

import { AppSettings } from './../urlset';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.page.html',
  styleUrls: ['./notification.page.scss'],
})
export class NotificationPage{
	image_url:any=AppSettings.image_url;     
    notifications:any=[];
    page:number=1;   

    show_notifications_please_wait:boolean=false;
    empty_data_message:boolean=false;

    notification_details={
        id:'',
        title:'',
        description:''
    }
	constructor(public api:ApiService,private _location: Location, private router: Router, public http:Http, public events:Events, public toastController: ToastController, private route: ActivatedRoute,public loadingController: LoadingController, public alertController:AlertController) {
    }
  	// Back button
	setBackButtonAction(){
	  this._location.back();
	}

	 //when this will load
    ionViewWillEnter(){     
        this.get_notifications();   
        /*let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if(currentUser === 'undefined' || currentUser === null){
            this.router.navigateByUrl('/login');
        }else{
            this.events.publish('checkLoggedInToken');
            setTimeout(()=>{
                this.get_notifications();
            },500);
        }*/
    }
    
     //get all parcel orders of current user's 
    get_notifications(){    
        this.show_notifications_please_wait=true;  
        this.page=1;   
        let data = {'page':this.page};        
        this.api.postApi('get_notifications',data,false).then(res => {
            let response : any = res;
            console.log("response",response)
            this.show_notifications_please_wait=false;
            if (response.status == true) {    
                this.notifications=response.notifications;
            }
            if(this.notifications.length<=0){
                this.empty_data_message=true;
            }else{
                this.empty_data_message=false;
            }
        }, error => {
            this.show_notifications_please_wait=false;
            console.log(error);
        });
    }  
    
    //when scroll down then append next notifications
    nextNotifications(infiniteScroll){
        setTimeout(() => {
            this.page++;               
            let data = {'page':this.page};        
            this.api.postApi('get_notifications',data,true).then(res => {
                let response : any = res;
                if (response.status == true) {    
                    this.notifications = this.notifications.concat(response.notifications);
                }
                infiniteScroll.target.complete();   
            }, error => {
                console.log(error);
            });
        }, 500);
    }
    
    //GET NOTIFICATION DETAILS based on notification id
    openNotificationDetails(notification_id, status, type_id){  
        if(status=='unread'){
            this.update_notification_read_by(notification_id);
        }   
        this.router.navigateByUrl('/product-details/'+type_id);
    }  
    
    //GET NOTIFICATION DETAILS based on notification id
    update_notification_read_by(notification_id){
        let requestData = { 'notification_id': notification_id};
        this.api.postApi('update_notification_read_by',requestData,true).then(res => {
            let response : any = res;
            console.log("updated",response)
            if (response.status == true) {
                setTimeout(()=>{
                    this.events.publish('updatenotificationCount');
                },500);
            }
        }, error => {
            console.log(error);
        });
    }

}

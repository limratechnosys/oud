import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-aboutus',
  templateUrl: './aboutus.page.html',
  styleUrls: ['./aboutus.page.scss'],
})
export class AboutusPage implements OnInit {

  constructor(private _location: Location,private iab: InAppBrowser) { }

  ngOnInit() {
  }
  // Back button
  setBackButtonAction(){
    this._location.back();
  }
  clickMe(){
    this.iab.create('http://www.gotitdelivery.com','_blank','location=yes, hardwareback=no');
  }
}

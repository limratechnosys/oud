import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CommentnotficationsPage } from './commentnotfications.page';

const routes: Routes = [
  {
    path: '',
    component: CommentnotficationsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CommentnotficationsPage]
})
export class CommentnotficationsPageModule {}

import { Component } from '@angular/core';
import { Events, ToastController, LoadingController, AlertController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Location } from '@angular/common';
import { ApiService } from '../service/api.service';
import { AppSettings } from './../urlset';
@Component({
  selector: 'app-commentnotfications',
  templateUrl: './commentnotfications.page.html',
  styleUrls: ['./commentnotfications.page.scss'],
})
export class CommentnotficationsPage {
    image_url:any=AppSettings.image_url;
    notifications:any=[];
    isshow : any = '0';
    page:number=1;
    showLoder : boolean = true;
    notification_details={
        id:'',
        title:'',
        description:''
    }
    constructor(private _location: Location, private router: Router, public http:Http, public events:Events, public toastController: ToastController, private route: ActivatedRoute,
    public loadingController: LoadingController, public alertController:AlertController,public api:ApiService) {
        this.events.subscribe('updatecommentNotificationlist',(()=>{
            this.getCommentnotificationList();
        }));
    }
    //when this will load
    ionViewWillEnter(){
        this.getCommentnotificationList();
    }
     //get all parcel orders of current user's
    async getCommentnotificationList(){
        let loading = await this.loadingController.create({
            message: 'Please wait...',
            cssClass: 'custom-loading'
        });
        loading.present();
        this.page=1;
        let data = {'page':this.page};
        this.api.postApi('getCommentnotificationList',data,true).then(res => {
		    let response : any = res;
		    loading.dismiss();
		    console.log("response",response)
		    if (response.status == true) {
		    	this.isshow = 1;
	            if (response.status == true) {
	                this.notifications=response.notifications;
	            }
		    }
		}); 
    }

    //when scroll down then append next notifications
    nextNotifications(infiniteScroll){
        setTimeout(() => {
        	this.page++;
        	let data = {'page':this.page};
        	this.api.postApi('getCommentnotificationList',{},true).then(res => {
			    let response : any = res;
			    if (response.status == true) {
	                this.notifications = this.notifications.concat(response.notifications);
	            }
	            infiniteScroll.target.complete();
            });
        }, 500);
    }

    //GET NOTIFICATION DETAILS based on notification id
    openNotificationDetails(typeID, commentId, status,catid){
        this.router.navigateByUrl('/category-details/'+catid+'/Product/'+typeID);
        if(status=='0'){
            //this.update_notification_read_by(commentId);
        }
    }

    //GET NOTIFICATION DETAILS based on notification id
    update_notification_read_by(commentId){
        let requestData = { 'commentId': commentId};
        this.api.postApi('update_comment_notification_read_by',requestData,true).then(res => {
		    let response : any = res;
		    if (response.status == true) {
                this.events.publish('updatehomePagedetails');
        	}
        });
       
    }
    // Back button
    setBackButtonAction(){
        this._location.back();
    }
}
import { Component,ElementRef } from '@angular/core';
import { AppSettings } from './../urlset';
import { ApiService } from '../service/api.service';
import { Events,LoadingController} from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import * as $ from 'jquery'
import { Location } from '@angular/common';

@Component({
  selector: 'app-my-collectable',
  templateUrl: './my-collectable.page.html',
  styleUrls: ['./my-collectable.page.scss'],
})
export class MyCollectablePage {
  image_url:any=AppSettings.image_url;
  upload_url:any=AppSettings.uploaded_image_url;
  products : any = [];
  prodcuthtml : any = '';
  showmsg : boolean = false;
  sliderConfig = {
    slidesPerView: 4,
    spaceBetween: 10,
    centeredSlides: false,
    autoplay:4
  };
  userdetails:any={
    id:0,
    name:'',
    image:''
  }
  constructor(
  	public api:ApiService,
  	public loadingController: LoadingController,
  	private router: Router,
  	public events:Events,
    public sanitizer: DomSanitizer,
    public el: ElementRef,
    private _location: Location) { 
    
  }
	async getproductlist(){
		let loading = await this.loadingController.create({
	    message: 'Please wait...',
	    cssClass: 'custom-loading'
  	});
  	loading.present();
  	this.api.postApi('get_products',{},true).then(res => {
	    let response : any = res;
      console.log("get_products response",response)
	    loading.dismiss();
	    if (response.status == true) {
        this.showmsg=true;
	    	this.products = response.products;
	    	console.log("response",response);
	    }
  	});    
	}

  productDetails(p_id,name){
    this.router.navigateByUrl('/category-details/'+p_id+'/'+name);
  }
   //when this will load
  ionViewWillEnter(){
      let currentUser = JSON.parse(localStorage.getItem('currentUser'));
      if(currentUser === 'undefined' || currentUser === null){
          this.router.navigateByUrl('/login/home');
      }else{
        let userdtls = JSON.parse(localStorage.getItem('user_details'));
        console.log("userdtls",userdtls)
        if(userdtls !== 'undefined' && userdtls !== null){
          this.userdetails=userdtls;
        }
        this.getproductlist();
      }  
  }
  // Back button
  setBackButtonAction(){
    this._location.back();
  }


}

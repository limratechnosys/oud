import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyCollectablePage } from './my-collectable.page';

describe('MyCollectablePage', () => {
  let component: MyCollectablePage;
  let fixture: ComponentFixture<MyCollectablePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyCollectablePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyCollectablePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, ViewChild } from '@angular/core';
import { Events,LoadingController,ToastController,AlertController} from '@ionic/angular';
import { AppSettings } from './../urlset';
import { ApiService } from '../service/api.service';
import { Location } from '@angular/common';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';

/*import { PostPopover } from './post-popover';
import { Messages } from '../messages/messages';
*/
@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.page.html',
  styleUrls: ['./timeline.page.scss'],
})
export class TimelinePage {
	image_url:any=AppSettings.image_url;
	upload_url:any=AppSettings.uploaded_image_url;
	loginUserid:number=0;
	arrdetails={
		userlist:[],
		timelist:[]
	};

	sliderConfig = {
		slidesPerView: 5,
		spaceBetween: 2,
		centeredSlides: false
	};
	hideInfiniteScroll:number=0;
	page:number=1;   
	moreRecords:any=[];
	type:any="";
	showLoader:boolean=true;
	searchdata:any="";
	constructor(
		public api:ApiService,  	
		public loadingController: LoadingController,
		public events:Events,
		public toastController: ToastController,
		private _location: Location,
		private socialSharing: SocialSharing,
		public sanitizer: DomSanitizer,
		private route: ActivatedRoute,
		private router: Router,
		public alertController: AlertController) {
		this.route.params.subscribe( params => {
	      this.type = params.type;  
	        
	    }); 
	}
	
	async gettimelinelist(){
		this.page=1;
		let currentUser = JSON.parse(localStorage.getItem('currentUser'));
	    let token=false;
	    if(currentUser !== 'undefined' && currentUser !== null){
	      token=true;
	    }
	    let loading;
	    if(this.showLoader){
			loading = await this.loadingController.create({
		    message: 'Please wait...',
		    cssClass: 'custom-loading'
			});
			loading.present();
		}
		let data ={'page':this.page,'searchdata':this.searchdata};
		this.api.postApi('gettimelinelist',data,true).then(res => {
		    let response : any = res;
		    if(this.showLoader){
		    	loading.dismiss();
		    }
		    console.log("response",response)
		    if (response.status == true) {
		    	this.arrdetails.userlist=response.userlist;
		    	this.arrdetails.timelist=response.timelist;
		    	this.loginUserid=response.uid;

		    }
		}); 
	}
	searchreacord(data){
		this.showLoader=false;
		this.searchdata=data;
		this.gettimelinelist();
	}


	 //when scroll down then append next banner products
   nexttimeList(infiniteScroll){
        this.page++;               
        let data ={'page':this.page,'searchdata':this.searchdata};
        this.api.postApi('gettimelinelist',data,true).then(res => {
		    let response : any = res;
		    console.log("response nexttimeList",response)
		    if (response.status == true) {
		    	this.moreRecords=response.timelist;
                this.arrdetails.timelist = this.arrdetails.timelist.concat(response.timelist);
                if(response.timelist.length<=0){
                    this.hideInfiniteScroll=1;
                }else{
                    this.hideInfiniteScroll=0;
                }
		    }
		    infiniteScroll.target.complete(); 
		}); 
    }


	 //share product details
    share_product_details(icnt){
        let file = this.upload_url+this.arrdetails.timelist[icnt].image;
        let productname = "Check this out "+this.arrdetails.timelist[icnt].name;
        let url = this.upload_url+"ProductDetails/" + this.arrdetails.timelist[icnt].id;
        this.socialSharing.share(productname, null, file, url);
    }

	favorateButton(icnt) {
        let data={ 'id': this.arrdetails.timelist[icnt].id};
        this.api.postApi('add_to_wishlist',data,true).then(res => {
		    let response : any = res;
		    if (response.status == true) {
	            this.events.publish('updatehomePagedetails');
	            this.arrdetails.timelist[icnt].is_favorate=1;
	            this.arrdetails.timelist[icnt].totalFavorate+=1;
	            
	        }else if(response.status == false){
	        	if(this.arrdetails.timelist[icnt].totalFavorate>0){
	        		this.arrdetails.timelist[icnt].totalFavorate-=1;
	        		this.arrdetails.timelist[icnt].is_favorate=0;
	        	}
	        }
		});
	}
	likeButton(icnt) {
        let data={ 'id': this.arrdetails.timelist[icnt].id};
        this.api.postApi('add_to_like',data,true).then(res => {
		    let response : any = res;
		    console.log("response",response)
		    if (response.status == true) {
	            this.arrdetails.timelist[icnt].is_like=1;
	            this.arrdetails.timelist[icnt].totalLikes+=1;
	            
	        }else if(response.status == false){
	        	if(this.arrdetails.timelist[icnt].totalLikes>0){
	        		this.arrdetails.timelist[icnt].totalLikes-=1;
	        		this.arrdetails.timelist[icnt].is_like=0;
	        	}
	        }
		});
	}
	
	// show message into toast
    async showMessage(type, message) {
        if(type=='success'){
            let successtoast = await this.toastController.create({
                message: message,
                duration: 3000,
                cssClass:'success-toast'
            });
            successtoast.present();
        }else{
             let failedtoast = await this.toastController.create({
                message: message,
                duration: 3000,
                cssClass:'failed-toast'
            });
            failedtoast.present();
        }
    }
    
	
	// Back button
    setBackButtonAction(){
      this._location.back();
    }
    ionViewWillEnter(){
    	this.gettimelinelist();
    }
    async copyproduct(id){
		const alert = await this.alertController.create({
			header: 'Copy',
			message: 'Are you sure you want to replicate this item to your collection?',
			buttons: [
			  {
			    text: 'Yes',
			    cssClass: 'btn-yes',
			    handler: () => {
			        this.router.navigateByUrl('/add-product/'+id+'/copy');
			    }
			  },
			  {
			    text: 'No',
			    role: 'cancel',
			    cssClass: 'btn-no',
			    handler: (blah) => {}
			  }
			]
		});
		await alert.present();

    	
    }
    //add-product/{{item.id}}/copy

}

